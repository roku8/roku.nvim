;; class
(class_declaration
  body: (class_body) @class.inner) @class.outer

(interface_declaration
  body: (interface_body) @class.inner) @class.outer

;; functions
(method_declaration) @function.outer

(method_declaration
  body: (block . "{" . (_) @_start @_end (_)? @_end . "}"
 (#make-range! "function.inner" @_start @_end)))

(constructor_declaration) @function.outer

(constructor_declaration
  body: (constructor_body . "{" . (_) @_start @_end (_)? @_end . "}"
 (#make-range! "function.inner" @_start @_end)))

;; loops
(for_statement
  body: (_)? @loop.inner) @loop.outer

(enhanced_for_statement
  body: (_)? @loop.inner) @loop.outer

(while_statement
  body: (_)? @loop.inner) @loop.outer

(do_statement
  body: (_)? @loop.inner) @loop.outer

;; conditionals
(switch_expression
  condition: (parenthesized_expression (_) @conditional.inner)
  ) @conditional.outer

(switch_block_statement_group) @conditional.outer

(switch_label (_) @conditional.inner)

(ternary_expression
  condition: (_) @conditional.inner) @conditional.outer

(if_statement
  condition: (parenthesized_expression (_) @conditional.inner))

(if_statement
  consequence: (block . "{" . (_) @_start (_)? @_end . "}"
                  (#make-range! "conditional.inner" @_start @_end)))

(if_statement
  alternative: (block . "{" . (_) @_start (_)? @_end . "}"
                  (#make-range! "conditional.inner" @_start @_end)))

(block (if_statement) @conditional.outer)

(while_statement 
  condition: (parenthesized_expression (_) @conditional.inner))

;; blocks
(block) @block.outer

(block . "{" . (_) @_start (_)? @_end . "}"
  (#make-range! "block.inner" @_start @_end))

(switch_block . "{" . (_) @_start (_)? @_end . "}"
  (#make-range! "block.inner" @_start @_end))

(lambda_expression
  body: (_) @block.inner)

;; calls
(method_invocation) @call.outer

(method_invocation
  object: ((_) . "." @_start)
  name: (_)
  arguments: (_) @_end
  (#make-range! "call.outer" @_start @_end))

(method_invocation
  arguments: (argument_list . "(" . (_) @_start (_)? @_end . ")"
  (#make-range! "call.inner" @_start @_end)))

;; parameters
(formal_parameters
  "," @_start .
  (formal_parameter) @parameter.inner
 (#make-range! "parameter.outer" @_start @parameter.inner))
(formal_parameters
  . (formal_parameter) @parameter.inner
  . ","? @_end
 (#make-range! "parameter.outer" @parameter.inner @_end))

(argument_list
  "," @_start .
  (_) @parameter.inner
 (#make-range! "parameter.outer" @_start @parameter.inner))
(argument_list
  . (_) @parameter.inner
  . ","? @_end
 (#make-range! "parameter.outer" @parameter.inner @_end))

;; comments
[
  (line_comment)
  (block_comment)
] @comment.outer

;; numbers
[
  (decimal_integer_literal)
  (decimal_floating_point_literal)
  (hex_integer_literal)
  (binary_integer_literal)
  (octal_integer_literal)
] @number.inner
