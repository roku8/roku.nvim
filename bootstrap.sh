#!/usr/bin/env bash

set -e

command -v jq
command -v yq

SCRIPT_DIR="$HOME/.config/nvim"
LAZYVERSION="$(jq '."lazy.nvim".commit' "$SCRIPT_DIR/lazy-lock.json" | tr -d '"')"
LAZYPATH="$HOME/.local/share/nvim/lazy/lazy.nvim"

rm -rf "$HOME/.local/share/nvim" "$HOME/.local/state/nvim" "$HOME/.cache/nvim"

git clone\
  --filter=blob:none\
  https://github.com/folke/lazy.nvim.git\
  --branch=stable\
  "$LAZYPATH"

cd "$LAZYPATH"
git checkout "$LAZYVERSION"
cd -

nvim -i NONE --headless +'qa!'
