vim.bo.textwidth = 100

require("which-key").add {
  { "<leader>m", group = "Markdown" },
  { "<leader>mp",         "<cmd>MarkdownPreview<cr>",       desc = "Open Browser Preview" },
  { "<leader>mr",         "<cmd>RenderMarkdown toggle<cr>", desc = "Toggle Beautify Markdown" }
}
