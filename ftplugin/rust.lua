local tools = {
  "rust-analyzer",
  "codelldb"
}

if not require("tools_installer").ensure_installed(tools) then
  return
end
