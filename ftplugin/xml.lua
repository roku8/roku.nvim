local installer = require("tools_installer")
local tools = {
  "lemminx",
  "lemminx-maven"
}

if not installer.ensure_installed(tools) then
  return
end

local lm_path = installer.get_install_path("lemminx-maven")
local lm_cmd = { "lemminx", "-cp", lm_path .. "/*" }
local lspsettings = require("lspsettings")
local projects = require("projects")
local config = {
  cmd = { "lemminx" },
  root_dir = projects.find_root({ ".git" }),
  capabilities = lspsettings.full_capabilities,
  on_attach = lspsettings.make_on_attach()
}

local bufnr = vim.api.nvim_get_current_buf()
local file = vim.api.nvim_buf_get_name(bufnr)
local filename = vim.fs.basename(file)

if filename == "pom.xml" then
  config.name = "lemminx-maven"
  config.cmd = lm_cmd
  config.root_dir = projects.find_root({ "mvnw", ".git" })
elseif filename:match(".*%.pom") then
  -- prefer attaching to an existing client since we usually get here by a jump to definition
  local lm_client = vim.lsp.get_clients({ name = "lemminx-maven" })[1]
  if lm_client then
    vim.lsp.buf_attach_client(bufnr, lm_client.id)
    return
  end
  config.name = "lemminx-maven"
  config.cmd = lm_cmd
  config.root_dir = vim.fn.getcwd()
end

vim.lsp.start(config)
