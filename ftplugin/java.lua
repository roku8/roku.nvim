local installer = require("tools_installer")
local tools = {
  "jdtls",
  "java-debug-adapter",
  "java-test",
  "google-java-format",
  "java-decompiler",
}

if not installer.ensure_installed(tools) then
  return
end

local jdtls = require("jdtls")

local function lsp_hover(err, result, ctx, config)
  config = config or { border = "rounded" }
  local contents = result.contents

  if type(contents) == "table" and contents[2] then
    local desc = string.gsub(contents[2], "%(jdt://.-%)", "()")
    local title = "```java\n" .. contents[1].value .. "\n```\n"

    contents[1].value = title .. "\n" .. desc
    contents[1].kind = "markdown"
    contents[2] = nil
  end

  return vim.lsp.handlers["textDocument/hover"](err, result, ctx, config)
end

local function extract_constant(visual)
  jdtls.extract_constant(visual)
  if visual then
    local keys = vim.api.nvim_replace_termcodes("<esc>", true, false, true)
    vim.api.nvim_feedkeys(keys, "in", false)
  end
end

local function extract_variable(visual)
  jdtls.extract_variable_all(visual)
  if visual then
    local keys = vim.api.nvim_replace_termcodes("<esc>", true, false, true)
    vim.api.nvim_feedkeys(keys, "in", false)
  end
end

local function extract_method(visual)
  jdtls.extract_method(visual)
  if visual then
    local keys = vim.api.nvim_replace_termcodes("<esc>", true, false, true)
    vim.api.nvim_feedkeys(keys, "in", false)
  end
end

local function get_bundles()
  local bundles = {}

  local debug_adp_path = installer.get_install_path("java-debug-adapter")
  table.insert(
    bundles,
    vim.fn.glob(debug_adp_path .. "/extension/server/com.microsoft.java.debug.plugin-*.jar", true)
  )

  local java_test_path = installer.get_install_path("java-test")
  vim.list_extend(bundles, vim.split(vim.fn.glob(java_test_path .. "/extension/server/*.jar", true), "\n"))

  local decompiler_path = installer.get_install_path("java-decompiler")
  vim.list_extend(bundles, vim.split(vim.fn.glob(decompiler_path .. "/server/*.jar", true), "\n"))

  return bundles
end

local home = os.getenv("HOME")
local project_root = require("projects").find_root({ "gradlew", "mvnw", ".git" })
local workspace_folder = home .. "/.local/cache/jdtls/" .. vim.fn.fnamemodify(project_root, ":p:h:t")
local jdtls_path = installer.get_install_path("jdtls")
local lombok_path = jdtls_path .. "/lombok.jar"
local lspsettings = require("lspsettings")

local config = {
  cmd = {
    "jdtls",
    "-data", workspace_folder,
    "--jvm-arg=-javaagent:" .. lombok_path
  },
  root_dir = project_root,
  capabilities = lspsettings.capabilities,
  init_options = {
    bundles = get_bundles(),
  },
  flags = {
    allow_incremental_sync = true,
  },
  handlers = {
    ["language/status"] = function() end,
    ["textDocument/hover"] = lsp_hover
  },
  commands = {
    ["java.show.references"] = function() vim.api.nvim_feedkeys("gr", "t", true) end,
    ["java.show.implementations"] = function() vim.api.nvim_feedkeys("gi", "t", true) end
  },
  on_attach = lspsettings.make_on_attach(function(client, bufnr)
    lspsettings.default_on_attach(client, bufnr)

    local wk = require("which-key")
    wk.add {
      buffer = bufnr,
      { "gs",         jdtls.super_implementation,                                          desc = "Goto Super Implementation" },
      { "<M-c>",      extract_constant,                                                    desc = "Extract Constant", },
      { "<M-v>",      extract_variable,                                                    desc = "Extract Variable", },
      { "<M-m>",      extract_method,                                                      desc = "Extract Method", },
      { "<leader>lc", jdtls.compile,                                                       desc = "Compile Project" },
      { "<leader>lu", jdtls.update_project_config,                                         desc = "Update Module Dependencies" },
      { "<leader>lU", function() jdtls.update_projects_config { select_mode = "all" } end, desc = "Update All Modules Dependencies" },
      { "<leader>lR", "<cmd>JdtWipeDataAndRestart<cr>",                                    desc = "Clear Cache and Restart LSP" },
      { "<leader>t",  group = "Test" },
      { "<leader>tc", jdtls.test_class,                                                    desc = "Test Class" },
      { "<leader>tm", jdtls.test_nearest_method,                                           desc = "Test Nearest Method" },
      { "<leader>tp", jdtls.pick_test,                                                     desc = "Pick Test Method" },
      { "<leader>tg", require("jdtls.tests").goto_subjects,                                desc = "Goto/Generate Test" }
    }

    wk.add {
      buffer = bufnr,
      -- these are a bit finicky, use with caution. Multiple tries may be needed
      { "<M-c>", function() extract_constant(true) end, desc = "Extract Constant", mode = "x" },
      { "<M-v>", function() extract_variable(true) end, desc = "Extract Variable", mode = "x" },
      { "<M-m>", function() extract_method(true) end,   desc = "Extract Method",   mode = "x" },
    }

    require("debugging").setup {
      project_root = project_root,
      buffer = bufnr
    }

    jdtls.setup_dap()
    require("jdtls.dap").setup_dap_main_class_configs()

    lspsettings.setup_lsp_hls(bufnr, {
      ["@lsp.mod.constant"] = function(token)
        local mod = token.modifiers
        return token.type == "property" and mod.static and mod.readonly
      end
    })

    vim.api.nvim_create_autocmd("InsertEnter", {
      buffer = bufnr,
      once = true,
      callback = function()
        require("cmp").setup.buffer {
          formatting = {
            expandable_indicator = false,
            fields = { "kind", "abbr", "menu" },
            format = require("lspkind").cmp_format {
              preset = "default",
              mode = "symbol",
              maxwidth = 30,
              ellipsis_char = "...",
            },
          },
        }
      end
    })
  end),
  settings = {
    java = {
      signatureHelp = { enabled = true },
      contentProvider = { preferred = 'fernflower' },
      completion = {
        matchCase = 'FIRSTLETTER',
        guessMethodArguments = true,
        favoriteStaticMembers = {
          "org.hamcrest.MatcherAssert.assertThat",
          "org.hamcrest.Matchers.*",
          "org.hamcrest.CoreMatchers.*",
          "org.junit.jupiter.api.Assertions.*",
          "java.util.Objects.requireNonNull",
          "java.util.Objects.requireNonNullElse",
          "java.util.AbstractMap.SimpleEntry",
          "java.util.AbstractMap.SimpleImmutableEntry",
          "org.mockito.Mockito.*",
          "org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*",
          "org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*",
          "org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*",
          "org.springframework.test.web.servlet.result.MockMvcResultMatchers.*",
          "org.springframework.test.web.servlet.result.MockMvcResultMatchers.*",
        },
        importOrder = {
          "#",
          ""
        }
      },
      sources = {
        organizeImports = {
          starThreshold = 9999,
          staticStarThreshold = 9999,
        },
      },
      codeGeneration = {
        toString = {
          template = "${object.className}{${member.name()}=${member.value}, ${otherMembers}}"
        }
      },
      maven = {
        downloadSources = true,
        updateSnapshots = true
      },
      implementationsCodeLens = {
        enabled = true,
      },
      referencesCodeLens = {
        enabled = true,
      },
      references = {
        includeDecompiledSources = true,
      },
      format = {
        enabled = false,
      },
      configuration = {
        updateBuildConfiguration = "interactive"
      }
    }
  }
}

config.settings.java = vim.tbl_deep_extend("force", config.settings.java, require("config").java)

jdtls.start_or_attach(config)
