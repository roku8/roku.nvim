local tools = {
  "typescript-language-server"
}

local is_angular = require("projects").find_root({ "angular.json" })
if is_angular then
  table.insert(tools, "angular-language-server")
end

if not require("tools_installer").ensure_installed(tools) then
  return
end

if is_angular then
  local bufnr = vim.api.nvim_get_current_buf()
  require("lspsettings").after_lsps_attached(
    bufnr,
    { "tsserver", "angularls" },
    function()
      require("which-key").add {
        { "<leader>lr", function() vim.lsp.buf.rename(nil, { name = "angularls" }) end, desc = "Rename Cursor Symbol", buffer = bufnr }
      }
    end
  )
end
