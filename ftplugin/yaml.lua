local tools = {
  "yaml-language-server"
}

if not require("tools_installer").ensure_installed(tools) then
  return
end
