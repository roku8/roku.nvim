#!/usr/bin/env bash

set -e

CWD=$(pwd)

cd "$HOME/.config/nvim"
git stash
git pull --ff-only

nvim -i NONE --headless +'qa!'
nvim -i NONE --headless +'Lazy! restore' +'qa!'

cd "$CWD"
