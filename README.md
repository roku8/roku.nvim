# Installation & Updates

## Requirements

* [nvim](https://neovim.io) 0.10.x
* git
* [ripgrep](https://github.com/BurntSushi/ripgrep)
* [fd](https://github.com/sharkdp/fd)
* [jq](https://jqlang.github.io/jq/download/)
* [yq](https://github.com/mikefarah/yq/#install)
* java >= 17
* nodejs >= 16 & npm
* curl/wget
* unzip
* tar
* gzip

## Installation Steps

```sh
git clone https://gitlab.com/roku8/roku.nvim ~/.config/nvim
~/.config/nvim/bootstrap.sh
```

## Updates

```sh
~/.config/nvim/update.sh
```

# Externalized Configuration

Some settings are system dependant. For example, the installation directory of a
jdk will likely be different on different machines. To accomodate this, neovim
will read a settings.json file from 2 locations:

* $HOME/.config/nvim/settings.json (Global settings)
* $PWD/.nvim/settings.json (Local settings - applies to the cwd only)

Any keys in the local settings.json will override those in the global settings.

Schema:

* guifont: string - the font to use in the gui
* colorscheme: string -  the colorscheme to use
* colorscheme_style: string - additional configuration for some colorschemes
* codelens_autorefresh: boolean - enable/disable autorefresh of codelens
* tools: map
    * key: name of a mason package
    * value: the version to install
* java: map - same as the default jdtls settings.java schema

Settings are reloaded when they are written, however they may not have any
effect if a plugin that reads them is already loaded. A restart may be required.

| Mode | Keymap  | Description |
| ---- | ------- | ----------- |
| n | `SPC ; ;`  | Edit global settings |
| n | `SPC ; ,`  | Edit local settings |

# Coding

LSP integration is enabled for the following languages:

* Java
* Lua (esp. for neovim configuration)
* XML (including pom.xml dependency autocomplete)
* Gradle
* JSON
* YAML
* Rust
* Javascript
* Typescript
* Angular
* HTML
* CSS
* BASH

Where possible, LSP specific plugins are added to extract the most out of the
language server.

## Automatic Tools Installation

Opening a file of a filetype that is configured to use a LSP for the first
time will trigger the installation of the following:

* required LSP(s)
* additional tools to augment the LSP (if configured)
* formatters (if configured)

When installation is triggered, a mason window pops up to show the progress of
installation. Wait for all the tools to be installed and then close the popup
(`<esc>` or `q`). Then reload the buffer with `:e` in order to start the LSP(s).

If you need a specific version of a tool to be installed, then specify the
version in settings.json under `tools`. The key is the name of the package as
identified by mason (run `:Mason` to get the name of the package) and the value
is the version you want installed. You should visit the package's homepage to
verify what version to specify.

## Navigation

| Mode | Keymap  | Description |
| ---- | ------- | ----------- |
| n | `g d`  | Go to definition |
| n | `g r`  | Go to references |
| n | `g t`  | Go to type definition |
| n | `g i`  | Go to implementation |
| n | `] m`  | Go to next method start |
| n | `[ m`  | Go to previous method start |
| n | `] M`  | Go to next method end |
| n | `[ M`  | Go to previous method end |
| n | `] f`  | Go to next method start |
| n | `[ f`  | Go to previous method start |
| n | `] F`  | Go to next method end |
| n | `[ F`  | Go to previous method end |
| n | `] c`  | Go to next conditional |
| n | `[ c`  | Go to previous conditional |
| n | `] b`  | Go to next block |
| n | `[ b`  | Go to previous block |
| n | `] l`  | Go to next loop |
| n | `[ l`  | Go to previous loop |
| n | `] p`  | Go to next parameter |
| n | `[ p`  | Go to previous parameter |
| n | `] ]`  | Go to next class/type |
| n | `[ [`  | Go to previous class/type |

## Code Objects

You can use motions similar to neovim's text objects (`:h text-objects`) to
operate on code objects like function blocks, conditionals, parameters etc.

The code object is defined by treesitter query captures. What each capture
represents depends on the language. Try them out and see what works best.

| Keymap Suffix | Treesitter Capture |
| ------------- | ---------------- |
| `a f` | @function.outer |
| `i f` | @function.inner |
| `a c` | @conditional.outer |
| `i c` | @conditional.inner |
| `a b` | @block.outer |
| `i b` | @block.inner |
| `a l` | @loop.outer |
| `i l` | @loop.inner |
| `a p` | @parameter.outer |
| `i p` | @parameter.inner |
| `a [` | @class.outer |
| `i [` | @class.inner |
| `a (` | @call.outer |
| `i (` | @call.inner (args list usually) |

## Incremental Code Object Selection

Code objects make it possible to quickly visual select a function definition
with the mapping `v a f` or a if-elseif-else block with `v a c` etc. However
this requires correct positioning of the cursor. For example, consider the
following block:

```java
if (something) {
//
} else {
    if (something-else) {
        // << you are here
    } else {
        //
    }
}
```

In the above, the cursor is in line 5. If you typed `v a c`, it would select the
if block from lines 4-8. If, however, you wanted to select the outermost if
block on line 1, you would have to position your cursor somewhere else like line
3.

Instead, it would be easier if you could just visually select whatever you're
currently on and then move the selection outwards (or inwards) incrementally to
select what you want.

| Mode | Keymap  | Description |
| ---- | ------- | ----------- |
| n | `v s`  | Begin incremental selection (can be used instead of code object) |
| v | `Tab`  | Move selection outwards |
| v | `Shift Tab`  | Move selection inwards |
| v | `Enter`  | Move selection to outer scope |

## LSP Actions

| Mode | Keymap  | Description |
| ---- | ------- | ----------- |
| n | `SPC l a`  | Execute a Code Action |
| n | `SPC l r`  | Rename the cursor symbol |
| n | `SPC l f`  | Format the current buffer |
| n | `SPC l s`  | Find a symbol in the current buffer |
| n | `SPC l S`  | Find a symbol in the workspace |
| n | `SPC l l`  | Show/Refresh Codelenses |
| n | `SPC l L`  | Execute a Codelens command |

## Autocompletion

Autocompletion provides results from the following sources in descending order
of priority:

* LSP
* Snippets
* Words from the current buffer
* Filesystem paths

Command line autocompletion is also enabled.

| Mode | Keymap  | Description |
| ---- | ------- | ----------- |
| i | `Tab` | Cycle forward through suggestions |
| i | `Shift Tab` | Cycle backward through suggestions |
| i | `Enter` | Confirm selected suggestion |
| i | `Ctrl e` | Cancel autocomplete (closes popup) |
| i | `Ctrl SPC` | Force trigger autocomplete |

## Debugging

Debugging support is also present for the following languages:

* Java
* Rust

You can set breakpoints, create watches and use familiar navigations like step
over, step into etc.

When you start debugging with at least one breakpoint set, the following windows pop up:

On the left (top-down):

* Breakpoints: shows the breakpoints currently set
* watches: tracks values of variables you want. Just insert the name of the
  variable.
* scopes: shows all variables defined in the current scope. you can hover over
  it with the cursor or expand it on `<CR>` to view it's value/state.
* stacks: The current thread call stack.

At the bottom:

* console: show program output and also holds the debug control buttons

If there are no breakpoints set - in order to simply run the program/test for eg., then there is
only the console window at the bottom.

In Debug mode, autocompletion is enabled in the following windows:

* DAP REPL
* DAPUI Watches
* DAPUI Hover

| Mode | Keymap  | Description |
| ---- | ------- | ----------- |
| n | `SPC d d`  | Start debugging / Run Application (if no breakpoints) |
| n | `SPC d b`  | Set a breakpoint on the current cursor line |
| n | `SPC d B`  | Clear all breakpoints |
| n | `SPC d u`  | Toggle the debug windows |
| n | `SPC d r`  | Toggle the dap repl window |
| n | `SPC d e`  | Evaluate the cursor word |
| n | `Down`  | Step Over |
| n | `Right`  | Step Into |
| n | `Up`  | Step Out |
| n | `Enter`  | Continue To Next Breakpoint |
| n | `Ctrl z`  | Terminate Debug Session |

## Java Language Support

Additional mappings are provided to take full advantage of jdtls - the preferred
LSP implementation for java.

| Mode | Keymap  | Description |
| ---- | ------- | ----------- |
| n | `Meta v` | Extract a variable |
| n | `Meta c` | Extract a constant |
| n | `Meta m` | Extract a method |
| v | `Meta v` | Extract a variable (from visual selection) |
| v | `Meta c` | Extract a constant (from visual selection) |
| v | `Meta m` | Extract a method (from visual selection) |
| n | `SPC l c` | Compile the whole project |
| n | `SPC l u` | Update dependencies for the current module |
| n | `SPC l U` | Update dependencies for the whole project |
| n | `SPC t p` | Choose a test to run |
| n | `SPC t m` | Run the current test method |
| n | `SPC t c` | Run the current test class |

**Notes**:

* If running linux, then `Meta` is `alt`. If running MacOS then `Meta` is `option`.
  Some terminals may require configuration to treat `option` key as `Meta`.
* *current module* is the module that contains the buffer in focus.
* To run a test, the cursor must be within a test class/method.

# Fuzzy Finding

Fuzzy find various things.

**Note**: File and word searches are relative to the cwd.

Command: `FzfLua`

Plugin: [fzf-lua](https://github.com/ibhagwan/fzf-lua)

| Mode | Keymap  | Description |
| ---- | ------- | ----------- |
| n | `SPC f f`  | Find a file       |
| n | `SPC f r`  | Find a file you opened previously sorted in order of recency |
| n | `SPC f g`  | Find a git tracked file |
| n | `SPC f w`  | Find a word       |
| n | `SPC f c`  | Find word under cursor |
| n | `SPC f b`  | Find an open buffer      |
| n | `SPC f p`  | Find a project     |
| n | `SPC f t`  | Find a Tabpage     |
| n | `SPC f SPC`  | Find Something Else (prefills the cmdline with :FzfLua) |

# Session Management

Quickly restore the previous editing state.

Any buffer, window or tabpage that the user opened
will be restored upon reloading a session.
However, buffers/windows/tabpages opened by plugins are not guaranteed to be
restored. Where possible additional handling logic is added to restore them
where possible. If they cannot be restored, the buffer/window/tabpage will be
deleted. Furthermore, terminal state will not be restored.

The current session is always saved before:

* Exiting neovim
* Loading a new session
* Detaching from the current session

One can detach from a session if they don't want subsequent editing state to be
saved.

| Mode | Keymap  | Description |
| ---- | ------- | ----------- |
| n | `SPC s s`  | Create/Save a session |
| n | `SPC s c`  | Create a session |
| n | `SPC s l`  | Load a session |
| n | `SPC s d`  | Delete a session |
| n | `SPC s x`  | Detach from the current session |

# Project Finder

The builtin project finder scans directories in the filesystem and identifies
those that have one or more project root markers in them. Selecting a project
opens it in a new tabpage.

Root markers are files that exist at the root directory of a project usually
required by some build system. For example, a pom.xml identifies the directory
as a maven project. Similarly a package.json - npm project etc.

Multi module projects usually have subdirectories that also contain the same
root marker but they are not to be considered as projects of their own. The
project finder handles this by excluding modules from search results.

The following root markers are currently supported:

* gradlew
* build.gradle
* settings.gradle
* build.gradle.kts
* settings.gradle.kts
* mvnw
* pom.xml
* Cargo.toml
* package.json
* Makefile
* lazy-lock.json

**Note**: hidden directories are not searched. This means that git repositories are
not currently considered as projects.

| Mode | Keymap  | Description |
| ---- | ------- | ----------- |
| n | `SPC f p`  | Find a project |

# Git

Command: Git
Pugin: [vim-fugitive](https://github.com/tpope/vim-fugitive)

* TODO

# Buffer Navigation

* TODO

# Filetree

Command: `Neotree`

| Mode | Keymap  | Description |
| ---- | ------- | ----------- |
| n | `Ctrl e`  | Toggle Filetree |

# Dashboard

* TODO

# Terminal

| Mode | Keymap  | Description |
| ---- | ------- | ----------- |
| n,t | `Ctrl t`  | Toggle Terminal |

# Convenience Mappings

## Clipboard

Neovim clipboard is integrated with system clipboard.

On Linux gui, `Ctrl c` and `Ctrl v` have their usual meanings in all modes.

On Linux terminal, it depends on the terminal (usually `Ctrl Shift c` and `Ctrl Shift v`).

On Macos, `Cmd c` and `Cmd v`.

## Window Movement

| Mode | Keymap  | Description |
| ---- | ------- | ----------- |
| n | `Ctrl j`  | Move to bottom window |
| n | `Ctrl k`  | Move to top window |
| n | `Ctrl h`  | Move to left window |
| n | `Ctrl l`  | Move to right window |

## Misc

| Mode | Keymap  | Description |
| ---- | ------- | ----------- |
| n | `SPC w`  | Save all buffers |
| n | `SPC q`  | Save all buffers and then quit neovim |
| n | `SPC /`  | Toggle comment line |
| n | `Q`  | Close current window |
| n | `E`  | Jump to end of line (exlcudes new line unlike $) |
| n | `B`  | Jump to beginning of line (first no blank character) |
| v | `<`  | Unindent selection (shift left) |
| v | `>`  | Indent selection (shift right) |
| v | `/`  | Toggle comment selection |

