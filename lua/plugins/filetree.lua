return {
  "nvim-neo-tree/neo-tree.nvim",
  branch = "v3.x",
  dependencies = {
    "nvim-lua/plenary.nvim",
    "nvim-tree/nvim-web-devicons",
    "MunifTanjim/nui.nvim",
  },
  cmd = "Neotree",
  config = function()
    require("neo-tree").setup {
      close_if_last_window = true,
      window = {
        position = "left",
      },
      filesystem = {
        follow_current_file = {
          enabled = true
        },
        group_empty_dirs = true,
        use_libuv_file_watcher = true,
        cwd_target = {
          current = "tab"
        }
      },
    }
  end
}
