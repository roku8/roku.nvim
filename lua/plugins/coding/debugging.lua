return {
  {
    "mfussenegger/nvim-dap",
    config = function()
      vim.fn.sign_define("DapBreakpoint", { text = " ", texthl = "DiagnosticError" })
      vim.fn.sign_define("DapBreakpointCondition", { text = "󰯲 ", texthl = "DiagnosticWarn" })
      vim.fn.sign_define("DapStopped", { text = " ", texthl = "DiagnosticInfo", linehl = "DebugCurrLine" })

      vim.api.nvim_create_autocmd('BufNewFile', {
        group = vim.api.nvim_create_augroup("nvim-dap-launch.json", { clear = true }),
        pattern = "*/.nvim/launch.json",
        callback = function(args)
          require("config").ensure_local_config_dir()
          local lines = {
            '{',
            '   "version": "0.2.0",',
            '   "configurations": [',
            '     {',
            '       "name": "<configuration-name>",',
            '       "type": "<adapter-name>",',
            '       "request": "launch"',
            '     }',
            '   ]',
            '}'
          }
          vim.api.nvim_buf_set_lines(args.buf, 0, -1, true, lines)
        end
      })

      local dap = require("dap")

      dap.adapters = require("debugging.adapters")

      dap.listeners.after.event_initialized["nvim_debug"] = function()
        local km_opts = { silent = true }
        vim.keymap.set("n", "<Down>", dap.step_over, km_opts)
        vim.keymap.set("n", "<Right>", dap.step_into, km_opts)
        vim.keymap.set("n", "<Up>", dap.step_out, km_opts)
        vim.keymap.set("n", "<C-Z>", dap.terminate, km_opts)
        vim.keymap.set("n", "<CR>", dap.continue, km_opts)
      end

      dap.listeners.after.event_terminated["nvim_debug"] = function()
        vim.keymap.del("n", "<Down>")
        vim.keymap.del("n", "<Right>")
        vim.keymap.del("n", "<Up>")
        vim.keymap.del("n", "<C-Z>")
        vim.keymap.del("n", "<CR>")
      end
    end
  },
  {
    "rcarriga/nvim-dap-ui",
    dependencies = {
      "mfussenegger/nvim-dap",
      "nvim-neotest/nvim-nio",
    },
    config = function()
      local dap = require("dap")
      local dapui = require("dapui")
      local debugging = require("debugging")

      dap.listeners.before.initialize["dapui_config"] = function()
        local buf = vim.fn.bufnr("DAP Console")
        if buf ~= -1 then
          vim.api.nvim_buf_delete(buf, { force = true })
        end

        local breakpoints = require("dap.breakpoints").get()
        if next(breakpoints) ~= nil then
          dapui.setup(debugging.dapui_layouts.default)
        else
          dapui.setup(debugging.dapui_layouts.no_breakpoints)
        end
      end

      local function close_filetree()
        if package.loaded["neo-tree"] then
          vim.cmd("Neotree close")
        end
      end

      local function close_terminal()
        require("terminal").close_termwin()
      end

      dap.listeners.after.event_initialized["dapui_config"] = function()
        close_filetree()
        close_terminal()
        dapui.open {
          reset = true
        }
      end
    end
  },
  {
    "rcarriga/cmp-dap",
    ft = { "dap-repl", "dapui_watches", "dapui_hover" },
    config = function()
      local cmp = require("cmp")

      cmp.setup.filetype({ "dap-repl", "dapui_watches", "dapui_hover" }, {
        enabled = function()
          return require("cmp_dap").is_dap_buffer()
        end,
        mapping = cmp.mapping.preset.insert({
          ["<CR>"] = cmp.mapping(function(fallback)
            fallback()
          end, { "i" }),
          ["<Tab>"] = cmp.mapping(function(fallback)
            if cmp.visible() then
              cmp.select_next_item()
            else
              fallback()
            end
          end, { "i", "s" }),
          ["<S-Tab>"] = cmp.mapping(function(fallback)
            if cmp.visible() then
              cmp.select_prev_item()
            else
              fallback()
            end
          end, { "i", "s" }),
        }),
        sources = {
          { name = "dap", max_item_count = 20 }
        }
      })
    end
  }
}
