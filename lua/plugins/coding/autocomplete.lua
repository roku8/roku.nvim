return {
  {
    "L3MON4D3/LuaSnip",
    version = "v2.*",
    build = "make install_jsregexp",
    dependencies = {
      "rafamadriz/friendly-snippets",
    },
    config = function()
      require("luasnip").setup {
        history = false,
        region_check_events = { "CursorMoved", "InsertEnter" },
        delete_check_events = { "InsertLeave" }
      }

      require("luasnip.loaders.from_vscode").lazy_load()
    end
  },
  {
    "hrsh7th/cmp-cmdline",
    dependencies = {
      "hrsh7th/nvim-cmp",
    },
    event = "CmdlineEnter",
    config = function()
      local cmp = require("cmp")

      cmp.setup.cmdline({ "/", "?" }, {
        mapping = cmp.mapping.preset.cmdline(),
        sources = { { name = "buffer", max_item_count = 20 } }
      })

      cmp.setup.cmdline(":", {
        mapping = cmp.mapping.preset.cmdline(),
        sources = cmp.config.sources(
          { { name = "cmdline", max_item_count = 20 } },
          { { name = "path" } }
        )
      })
    end
  },
  {
    "saadparwaiz1/cmp_luasnip",
    dependencies = {
      "hrsh7th/nvim-cmp",
      "L3MON4D3/LuaSnip",
    },
    event = "InsertEnter",
    config = function()
      local cmp = require("cmp")
      local luasnip = require("luasnip")

      cmp.setup {
        snippet = {
          expand = function(args)
            luasnip.lsp_expand(args.body)
          end,
        },
        mapping = cmp.mapping.preset.insert({
          ["<Tab>"] = cmp.mapping(function(fallback)
            if cmp.visible() then
              cmp.select_next_item()
            elseif luasnip.expand_or_locally_jumpable() then
              luasnip.expand_or_jump()
            else
              fallback()
            end
          end, { "i", "s" }),
          ["<S-Tab>"] = cmp.mapping(function(fallback)
            if cmp.visible() then
              cmp.select_prev_item()
            elseif luasnip.locally_jumpable(-1) then
              luasnip.jump(-1)
            else
              fallback()
            end
          end, { "i", "s" }),
        })
      }
    end
  },
  { "hrsh7th/cmp-nvim-lsp" },
  {
    "hrsh7th/nvim-cmp",
    dependencies = {
      "hrsh7th/cmp-buffer",
      "hrsh7th/cmp-path",
      "onsails/lspkind.nvim"
    },
    config = function()
      local cmp = require("cmp")

      cmp.setup {
        view = {
          entries = { name = "custom", selection_order = "near_cursor" }
        },
        preselect = cmp.PreselectMode.None,
        window = {
          completion = cmp.config.window.bordered({
            winhighlight = ""
          }),
          documentation = cmp.config.window.bordered({
            winhighlight = ""
          }),
        },
        mapping = cmp.mapping.preset.insert({
          ["<C-u>"] = cmp.mapping.scroll_docs(-1),
          ["<C-d>"] = cmp.mapping.scroll_docs(1),
          ["<C-Space>"] = cmp.mapping.complete(),
          ["<CR>"] = cmp.mapping.confirm(),
          ["<Tab>"] = cmp.mapping(function(fallback)
            if cmp.visible() then
              cmp.select_next_item()
            else
              fallback()
            end
          end, { "i", "s" }),
          ["<S-Tab>"] = cmp.mapping(function(fallback)
            if cmp.visible() then
              cmp.select_prev_item()
            else
              fallback()
            end
          end, { "i", "s" }),
        }),
        sources = {
          { name = "nvim_lsp", priority = 4, max_item_count = 20 },
          { name = "luasnip",  priority = 3, max_item_count = 5 },
          { name = "buffer",   priority = 2, max_item_count = 10 },
          { name = "path",     priority = 1 },
        },
        formatting = {
          expandable_indicator = false,
          fields = { "kind", "abbr", "menu" },
          format = require("lspkind").cmp_format {
            preset = "default",
            mode = "symbol",
            maxwidth = 30,
            ellipsis_char = "...",
            before = function(entry, vim_item)
              vim_item.menu = entry:get_completion_item().detail
              return vim_item
            end
          },
        },
        experimental = {
          ghost_text = { hl_group = "CmpGhostText" }
        },
      }
    end
  }
}
