return {
  require("plugins.coding.languages.neovim"),
  require("plugins.coding.languages.java"),
  require("plugins.coding.languages.rust"),
  require("plugins.coding.languages.angular"),
}
