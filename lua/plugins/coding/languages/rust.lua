return {
  {
    "simrat39/rust-tools.nvim",
    dependencies = { "nvim-lspconfig" },
    ft = "rust",
    config = function()
      local codelldb_path = require("tools_installer").get_install_path("codelldb")
      local codelldb_bin = codelldb_path .. "/codelldb"
      local osname = vim.uv.os_uname().sysname
      local lldb_lib = codelldb_path .. "/extension/lldb/lib/liblldb" .. (osname == "Linux" and ".so" or ".dylib")
      local lspsettings = require("lspsettings")

      require("rust-tools").setup {
        tools = {
          inlay_hints = {
            show_parameter_hints = false,
          }
        },
        server = {
          capabilities = lspsettings.capabilities,
          on_attach = function(client, bufnr)
            lspsettings.default_on_attach(client, bufnr)
            require("debugging").setup {
              project_root = require("projects").find_root({ "Cargo.toml", ".git" }),
              buffer = bufnr,
              adapter = "codelldb"
            }
          end,
        },
        dap = {
          adapter = require("rust-tools.dap").get_codelldb_adapter(codelldb_bin, lldb_lib)
        }
      }
    end,
  },
  {
    "saecki/crates.nvim",
    dependencies = { "nvim-lua/plenary.nvim" },
    event = "BufRead Cargo.toml",
    config = function()
      require("crates").setup {
        popup = {
          border = "rounded"
        },
        null_ls = {
          enabled = true,
          name = "crates"
        },
        on_attach = function(bufnr)
          local wk = require("which-key")
          wk.add {
            buffer = bufnr,
            { "<leader>l", group = "Language" },
            { "<leader>la", vim.lsp.buf.code_action, desc = "Code Action" }
          }
        end
      }

      vim.api.nvim_create_autocmd("InsertEnter", {
        pattern = "<buffer>",
        once = true,
        callback = function()
          require("cmp").setup.buffer {
            sources = {
              { name = "crates", priority = 2, max_item_count = 20 },
              { name = "buffer", priority = 1, max_item_count = 10 }
            }
          }
        end
      })
    end,
  },
}
