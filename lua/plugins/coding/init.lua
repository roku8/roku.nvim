return {
  require("plugins.coding.syntax"),
  require("plugins.coding.languages"),
  require("plugins.coding.lsp"),
  require("plugins.coding.tools_installer"),
  require("plugins.coding.autocomplete"),
  require("plugins.coding.debugging"),
}
