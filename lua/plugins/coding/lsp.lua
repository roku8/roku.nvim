return {
  {
    "neovim/nvim-lspconfig",
    event = "BufReadPre",
    config = function()
      vim.fn.sign_define("DiagnosticSignError", { text = "", texthl = "DiagnosticSignError" })
      vim.fn.sign_define("DiagnosticSignWarn", { text = "", texthl = "DiagnosticSignWarn" })
      vim.fn.sign_define("DiagnosticSignInfo", { text = "", texthl = "DiagnosticSignInfo" })
      vim.fn.sign_define("DiagnosticSignHint", { text = "", texthl = "DiagnosticSignHint" })

      vim.lsp.handlers["textDocument/hover"] = vim.lsp.with(vim.lsp.handlers.hover, {
        border = "rounded",
        title = "Documentation"
      })

      vim.lsp.handlers["textDocument/signatureHelp"] = vim.lsp.with(vim.lsp.handlers.signature_help, {
        border = "rounded"
      })

      vim.diagnostic.config {
        float = { border = "rounded" }
      }

      local lspconfig = require("lspconfig")
      local lspsettings = require("lspsettings")

      for name, config in pairs(lspsettings.servers) do
        config.capabilities = lspsettings.capabilities
        config.on_attach = lspsettings.make_on_attach(config.on_attach)
        lspconfig[name].setup(config)
      end

      require("lspconfig.ui.windows").default_options.border = "rounded"
    end
  },
  {
    "jose-elias-alvarez/null-ls.nvim",
    event = "BufReadPre",
    dependencies = { "nvim-lua/plenary.nvim" },
    module = false,
    config = function()
      local null_ls = require("null-ls")
      null_ls.setup {
        sources = {
          null_ls.builtins.formatting.google_java_format,
          null_ls.builtins.formatting.rustfmt,
        },
      }
    end
  },
  {
    "j-hui/fidget.nvim",
    tag = "legacy",
    pin = true,
    event = "LspAttach",
    config = function()
      require("fidget").setup {
        text = {
          spinner = "dots"
        },
        window = {
          blend = 0,
        },
        fmt = {
          max_width = 20,
        },
      }
    end
  }
}
