return {
  {
    "nvim-treesitter/nvim-treesitter",
    dependencies = {
      "nvim-treesitter/nvim-treesitter-textobjects",
    },
    lazy = vim.fn.argc() == 0,
    event = "VeryLazy",
    config = function()
      ---@diagnostic disable-next-line: missing-fields
      require("nvim-treesitter.configs").setup {
        ensure_installed = {
          "c",
          "cpp",
          "meson",
          "lua",
          "query",
          "vim",
          "vimdoc",
          "java",
          "xml",
          "graphql",
          "json",
          "yaml",
          "sql",
          "rust",
          "toml",
          "markdown",
          "markdown_inline",
          "bash",
          "typescript",
          "javascript",
          "html",
          "css",
          "scss",
          "gitcommit",
          "gitignore",
          "git_rebase"
        },
        sync_install = false,
        highlight = {
          enable = true,
        },
        incremental_selection = {
          enable = true,
          keymaps = {
            init_selection = "vs",
            node_incremental = "<Tab>",
            scope_incremental = "<CR>",
            node_decremental = "<S-Tab>",
          },
        },
        textobjects = {
          select = {
            enable = true,
            keymaps = {
              ["af"] = "@function.outer",
              ["if"] = "@function.inner",
              ["ac"] = "@conditional.outer",
              ["ic"] = "@conditional.inner",
              ["ab"] = "@block.outer",
              ["ib"] = "@block.inner",
              ["al"] = "@loop.outer",
              ["il"] = "@loop.inner",
              ["ap"] = "@parameter.outer",
              ["ip"] = "@parameter.inner",
              ["a["] = "@class.outer",
              ["i["] = "@class.inner",
              ["a("] = "@call.outer",
              ["i("] = "@call.inner",
            },
          },
          move = {
            enable = true,
            goto_next_start = {
              ["]m"] = "@function.outer",
              ["]f"] = "@function.outer",
              ["]c"] = "@conditional.outer",
              ["]b"] = "@block.outer",
              ["]l"] = "@loop.outer",
              ["]p"] = "@parameter.inner",
              ["]]"] = "@class.outer"
            },
            goto_next_end = {
              ["]M"] = "@function.outer",
              ["]F"] = "@function.outer",
            },
            goto_previous_start = {
              ["[m"] = "@function.outer",
              ["[f"] = "@function.outer",
              ["[c"] = "@conditional.outer",
              ["[b"] = "@block.outer",
              ["[l"] = "@loop.outer",
              ["[p"] = "@parameter.inner",
              ["[["] = "@class.outer"
            },
            goto_previous_end = {
              ["[M"] = "@function.outer",
              ["[F"] = "@function.outer",
            },
          }
        }
      }

      vim.cmd("silent TSUpdate")

      vim.wo.foldmethod = "expr"
      vim.wo.foldexpr = "nvim_treesitter#foldexpr()"
      vim.wo.foldenable = false
    end
  },
}
