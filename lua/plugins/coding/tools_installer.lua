return {
  {
    "williamboman/mason.nvim",
    version = "*",
    build = ":MasonUpdate",
    event = "VeryLazy",
    config = function()
      require("mason").setup {
        ui = {
          border = "rounded"
        },
        registries = {
          "file:~/.config/nvim/mason-extras",
          "github:mason-org/mason-registry",
        },
      }

      require("mason-registry").update()
    end
  },
}
