return {
  {
    "romgrk/barbar.nvim",
    version = "^1.0.0",
    dependencies = "nvim-tree/nvim-web-devicons",
    event = "BufRead",
    init = function()
      vim.g.barbar_auto_setup = false
    end,
    config = function()
      require("barbar").setup {
        animation = false,
        focus_on_close = "previous",
        maximum_padding = 1,
        tabpages = true,
        insert_at_end = true,
        icons = {
          buffer_index = true,
          diagnostics = {
            [vim.diagnostic.severity.ERROR] = { enabled = true },
          },
          pinned = { button = "", filename = true },
        },
      }
    end
  },
  {
    "tiagovla/scope.nvim",
    event = "BufAdd",
    config = function()
      require("scope").setup {
        hooks = {
          pre_tab_leave = function()
            vim.api.nvim_exec_autocmds('User', { pattern = 'ScopeTabLeavePre' })
          end,
          post_tab_enter = function()
            vim.api.nvim_exec_autocmds('User', { pattern = 'ScopeTabEnterPost' })
          end,
        },
      }
    end
  }
}
