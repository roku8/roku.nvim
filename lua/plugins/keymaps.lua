return {
  "folke/which-key.nvim",
  event = "VeryLazy",
  config = function()
    require("which-key").setup {
      win = {
        border = "rounded"
      },
      icons = {
        mappings = false
      }
    }

    require("keymaps").setup()
  end
}
