return {
  "rohit-s8/floating-input.nvim",
  event = "VeryLazy",
  config = function()
    vim.ui.input = function(opts, on_confirm)
      require("floating-input").input(opts, on_confirm, { title_pos = "center" })
    end
  end
}
