return {
  "ibhagwan/fzf-lua",
  dependencies = {
    "nvim-tree/nvim-web-devicons",
    {
      "junegunn/fzf",
      version = "0.*",
      build = "./install --bin"
    }
  },
  cmd = "FzfLua",
  config = function()
    require("fzf-lua").setup {
      winopts     = {
        width = 0.85,
        preview = {
          layout = "vertical",
          vertical = "up:45%",
          title_align = "center"
        },
      },
      fzf_opts    = {
        ["--info"] = "default",
        ["--no-separator"] = "",
        ["--pointer"] = " ",
        ["--marker"] = "+ ",
      },
      fzf_colors  = require("fzf_colors"),
      files       = {
        prompt = "❯ ",
        rg_opts = "--color=never --files --hidden -g '!.git' -g '!.m2' -g '!node_modules'",
        fd_opts = "--color=never --type f --hidden --exclude .git --exclude .m2 --exclude node_modules",
        winopts = {
          title = "Files",
          title_pos = "center"
        }
      },
      oldfiles    = {
        prompt = "❯ ",
        cwd_only = true,
        include_current_session = true,
        winopts = {
          title = "Recent Files",
          title_pos = "center"
        }
      },
      git         = {
        files = {
          prompt = "❯ ",
          winopts = {
            title = "Git Files",
            title_pos = "center"
          }
        },
        commits = {
          prompt = "❯ ",
          winopts = {
            title = "Git Commits",
            title_pos = "center"
          }
        },
        branches = {
          prompt = "❯ ",
          winopts = {
            title = "Git Branches",
            title_pos = "center"
          }
        }
      },
      buffers     = {
        prompt = "❯ ",
        winopts = {
          title = "Buffers",
          title_pos = "center"
        }
      },
      tabs        = {
        prompt = "❯ ",
        winopts = {
          title = "Tabs",
          title_pos = "center",
          width = 0.5,
          height = 0.33,
        },
        previewer = false
      },
      grep        = {
        prompt = "❯ ",
        winopts = {
          title = "Grep",
          title_pos = "center"
        },
      },
      lsp         = {
        prompt_postfix = "❯ ",
        jump_to_single_result = true,
        includeDeclaration = false,
        winopts = {
          title = "LSP",
          title_pos = "center"
        },
        symbols = {
          prompt  = "❯ ",
          winopts = {
            title = "Symbols",
            title_pos = "center"
          },
        },
        code_actions = {
          prompt    = "❯ ",
          previewer = false,
          winopts   = {
            title = "Code Actions",
            title_pos = "center",
            width = 0.5,
            height = 0.5,
          },
        },
        finder = {
          prompt = "❯ ",
          winopts = {
            title = "LSP Finder",
            title_pos = "center"
          },
        }
      },
      diagnostics = {
        prompt = "❯ ",
        cwd_only = true,
        winopts = {
          title = "Diagnostics",
          title_pos = "center"
        },
      }
    }

    require("projects").setup()
  end
}
