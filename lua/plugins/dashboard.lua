return {
  "goolord/alpha-nvim",
  dependencies = { "nvim-tree/nvim-web-devicons" },
  pin = true,
  event = "VimEnter",
  config = function()
    require("dashboard").setup("start")

    vim.api.nvim_create_autocmd("User", {
      pattern = "AlphaReady",
      callback = function()
        vim.o.showtabline = 0
      end
    })

    vim.api.nvim_create_autocmd("User", {
      pattern = "AlphaClosed",
      callback = function()
        vim.o.showtabline = 2
      end
    })

    vim.api.nvim_create_autocmd("User", {
      pattern = "SessionDeleted",
      callback = function ()
        local buf = vim.api.nvim_get_current_buf()
        if vim.api.nvim_get_option_value("filetype", { buf = buf }) == "alpha" then
          require("dashboard").refresh()
        end
      end
    })
  end,
}
