return {
  "catppuccin/nvim",
  name = "catppuccin",
  pin = true,
  priority = 1000,
  lazy = false,
  config = function()
    require("catppuccin").setup {
      flavour = "mocha",
      compile_path = vim.fn.stdpath("cache") .. "/catppuccin",
      term_colors = true,
      integrations = {
        cmp = true,
        gitsigns = true,
        treesitter = true,
        mason = true,
        native_lsp = {
          enabled = false
        }
      },
      custom_highlights = function(colors)
        return {
          -- syntax highlights
          ["String"] = { fg = colors.green },
          ["Number"] = { fg = colors.maroon },
          ["Boolean"] = { fg = colors.maroon },
          ["Operator"] = { fg = colors.overlay1 },
          ["Keyword"] = { fg = colors.red },
          ["Conditional"] = { link = "Keyword" },
          ["Repeat"] = { link = "Keyword" },
          ["Include"] = { link = "Keyword" },
          ["Exception"] = { link = "Keyword" },
          ["StorageClass"] = { link = "Keyword" },
          ["Type"] = { fg = colors.yellow },
          ["Structure"] = { link = "Type" },
          ["Identifier"] = { fg = colors.text },
          ["Constant"] = { link = "Identifier" },
          ["Tag"] = { fg = colors.red },
          ["Delimiter"] = { fg = colors.overlay2 },

          -- treesitter highlights
          ["@keyword.return"] = { link = "@keyword" },
          ["@keyword.operator"] = { link = "@keyword" },
          ["@keyword.function"] = { link = "@keyword" },
          ["@keyword.conditional"] = { link = "@keyword" },
          ["@keyword.repeat"] = { link = "@keyword" },
          ["@keyword.exception"] = { link = "@keyword" },
          ["@keyword.import"] = { link = "@keyword" },
          ["@attribute"] = { link = "@keyword" },
          ["@type.builtin"] = { link = "@keyword" },
          ["@type.builtin.c"] = { link = "@keyword" },
          ["@type.qualifier"] = { link = "@keyword" },
          ["@field"] = { fg = colors.text, italic = true },
          ["@variable"] = { link = "Identifier" },
          ["@variable.member"] = { link = "@field" },
          ["@property"] = { link = "@field" },
          ["@constant"] = { link = "@variable" },
          ["@constant.java"] = { link = "@constant" },
          ["@constant.builtin"] = { link = "@keyword" },
          ["@variable.builtin"] = { link = "@keyword" },
          ["@parameter"] = { fg = colors.peach },
          ["@function.builtin"] = { link = "@variable.builtin" },
          ["@function.macro"] = { link = "@macro" },
          ["@comment.documentation"] = { fg = colors.subtext1, bold = true },
          ["@tag"] = { fg = colors.red },
          ["@tag.delimiter"] = { link = "Delimiter" },
          ["@tag.attribute"] = { fg = colors.sapphire, italic = true },

          -- semantic highlights
          ["@lsp.type.interface"] = { fg = colors.green },
          ["@lsp.type.annotation"] = { link = "@attribute" },
          ["@lsp.type.class"] = { link = "@type" },
          ["@lsp.type.struct"] = { link = "@type" },
          ["@lsp.type.method"] = { link = "@function" },
          ["@lsp.type.parameter"] = { link = "@parameter" },
          ["@lsp.type.variable"] = { link = "@variable" },
          ["@lsp.type.enum"] = { link = "@type" },
          ["@lsp.type.enumMember"] = { fg = colors.lavender, bold = true },
          ["@lsp.type.typeParameter"] = { link = "@parameter" },
          ["@lsp.type.property"] = { link = "@property" },
          ["@lsp.type.macro"] = { link = "@function.macro" },
          ["@lsp.type.comment"] = { link = "@lsp" },
          ["@lsp.mod.deprecated"] = { fg = colors.overlay1, strikethrough = true },
          ["@lsp.mod.constant"] = { bold = true },
          ["@lsp.typemod.variable.global"] = { link = "@variable.builtin" },
          ["@lsp.typemod.method.static"] = { italic = true },
          ["@lsp.typemod.method.defaultLibrary"] = { link = "@lsp" },
          ["@lsp.typemod.class.abstract"] = { link = "@lsp.type.interface" },
          ["@lsp.typemod.macro.defaultLibrary"] = { link = "@lsp" },

          -- floating windows
          NormalFloat = { fg = colors.text, bg = colors.base },
          FloatTitle = { fg = colors.peach, bold = true },
          FloatBorder = { fg = colors.surface0 },

          -- lsp & diagnostics
          LspCodeLens = { link = "@comment" },
          LspInfoBorder = { link = "FloatBorder" },
          DiagnosticHint = { fg = colors.overlay1, italic = true },
          DiagnosticInfo = { fg = colors.teal, italic = true },
          DiagnosticWarn = { fg = colors.yellow, italic = true },
          DiagnosticError = { fg = colors.red, italic = true },
          DiagnosticDeprecated = { link = "@lsp.mod.deprecated" },

          -- dap
          DebugCurrLine = { bg = colors.surface2 },

          -- cmp
          CmpGhostText = { fg = colors.surface2 },
          PmenuThumb = { bg = colors.overlay0 },

          -- fzf-lua
          FzfLuaBorder = { link = "FloatBorder" },
          FzfLuaTitle = { link = "FloatTitle" },

          -- neo-tree
          NeoTreeTitleBar = { fg = colors.base, bg = colors.text },
          NeoTreeGitUntracked = { fg = colors.peach },

          -- alpha
          AlphaHeader = { fg = colors.overlay0 },
          AlphaHeaderLabel = { fg = colors.peach },
          AlphaButtons = { fg = colors.blue },
          AlphaShortcut = { fg = colors.green },

          -- whick-key
          WhichKeyGroup = { fg = colors.mauve },
          WhichKeySeparator = { fg = colors.red },

          -- misc
          Session = { fg = colors.green },
          DiffText = { reverse = true },
        }
      end
    }
  end
}
