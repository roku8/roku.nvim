return {
  {
    "lewis6991/gitsigns.nvim",
    event = "BufRead",
    config = function()
      require("gitsigns").setup {
        preview_config = {
          border = "rounded"
        }
      }
    end
  },
  {
    "tpope/vim-fugitive",
    cmd = "Git",
    config = function()
      vim.api.nvim_create_autocmd("Filetype", {
        pattern = "fugitive",
        callback = function(ev)
          vim.api.nvim_set_option_value("buflisted", false, { buf = ev.buf })
        end
      })
    end
  },
  {
    "sindrets/diffview.nvim",
    dependencies = { "nvim-lua/plenary.nvim" },
    cmd = { "DiffviewOpen", "DiffviewFileHistory" },
    config = function()
      local actions = require("diffview.config").actions
      require("diffview").setup {
        view = {
          merge_tool = {
            layout = "diff3_mixed"
          }
        },
        keymaps = {
          disable_defaults = true,
          view = {
            { "n", "<C-e>",   actions.focus_files,  { desc = "Open/Focus File Panel" } },
            { "n", "<C-M-l>", actions.cycle_layout, { desc = "Cycle Available Layouts" } },
            { "n", "g?",      actions.help("view"), { desc = "Diffview Help " } },
          },
          diff_view = {
            { "n", "g?", actions.help({ "view", "merge_tool" }), { desc = "Diffview Help " } },
          },
          file_panel = {
            { "n", "<C-e>",  actions.close,                         { desc = "Close File Panel" } },
            { "n", "j",      actions.next_entry,                    { desc = "Next Entry" } },
            { "n", "<down>", actions.next_entry,                    { desc = "Next Entry" } },
            { "n", "k",      actions.prev_entry,                    { desc = "Previous Entry" } },
            { "n", "<up>",   actions.prev_entry,                    { desc = "Previous Entry" } },
            { "n", "<cr>",   actions.select_entry,                  { desc = "Select Entry" } },
            { "n", "-",      actions.toggle_stage_entry,            { desc = "Stage/Unstage Entry" } },
            { "n", "X",      actions.restore_entry,                 { desc = "Restore Entry" } },
            { "n", "xo",     actions.conflict_choose_all("ours"),   { desc = "Choose OURS version for all conflicts" } },
            { "n", "xt",     actions.conflict_choose_all("theirs"), { desc = "Choose THEIRS version for all conflicts" } },
            { "n", "xb",     actions.conflict_choose_all("base"),   { desc = "Choose BASE version for all conflicts" } },
            { "n", "xa",     actions.conflict_choose_all("all"),    { desc = "Choose ALL versions for all conflicts" } },
            { "n", "xd",     actions.conflict_choose_all("none"),   { desc = "Delete all conflicts" } },
            { "n", "g?",     actions.help("file_panel"),            { desc = "Diffview Help " } },
          },
          file_history_panel = {
            { "n", "<C-e>",  actions.close,                      { desc = "Close File Panel" } },
            { "n", "j",      actions.next_entry,                 { desc = "Next Entry" } },
            { "n", "<down>", actions.next_entry,                 { desc = "Next Entry" } },
            { "n", "k",      actions.prev_entry,                 { desc = "Previous Entry" } },
            { "n", "<up>",   actions.prev_entry,                 { desc = "Previous Entry" } },
            { "n", "<cr>",   actions.select_entry,               { desc = "Select Entry" } },
            { "n", "y",      actions.copy_hash,                  { desc = "Copy Commit Hash" } },
            { "n", "X",      actions.restore_entry,              { desc = "Restore File To Selected Commit State" } },
            { "n", "L",      actions.open_commit_log,            { desc = "Open Commit Log" } },
            { "n", "g?",     actions.help("file_history_panel"), { desc = "Diffview Help " } },
          },
          diff1 = {
            { "n", "g?", actions.help({ "view", "diff1" }), { desc = "Diffview Help" } },
          },
          diff2 = {
            { "n", "g?", actions.help({ "view", "diff2" }), { desc = "Diffivew Help" } },
          },
          diff3 = {
            { "n", "]x",  actions.next_conflict,             { desc = "Next conflict" } },
            { "n", "[x",  actions.prev_conflict,             { desc = "Previous conflict" } },
            { "n", "xo",  actions.conflict_choose("ours"),   { desc = "Choose OURS version of cursor conflict" } },
            { "n", "xt",  actions.conflict_choose("theirs"), { desc = "Choose THEIRS version of cursor conflict" } },
            { "n", "xa",  actions.conflict_choose("all"),    { desc = "Choose ALL versions of cursor conflict" } },
            { "n", "xd",  actions.conflict_choose("none"),   { desc = "Delete the conflict region" } },
            { "n", "2do", actions.diffget("ours"),           { desc = "Obtain OURS version of cursor diff" } },
            { "n", "3do", actions.diffget("theirs"),         { desc = "Obtain THEIRS version of cursor diff" } },
            { "n", "g?",  actions.help({ "view", "diff3" }), { desc = "Diffview Help" } },
          },
          diff4 = {
            { "n", "]x",  actions.next_conflict,             { desc = "Next conflict" } },
            { "n", "[x",  actions.prev_conflict,             { desc = "Previous conflict" } },
            { "n", "xo",  actions.conflict_choose("ours"),   { desc = "Choose OURS version of cursor conflict" } },
            { "n", "xt",  actions.conflict_choose("theirs"), { desc = "Choose THEIRS version of cursor conflict" } },
            { "n", "xb",  actions.conflict_choose("base"),   { desc = "Choose BASE version of cursor conflict" } },
            { "n", "xa",  actions.conflict_choose("all"),    { desc = "Choose ALL versions of cursor conflict" } },
            { "n", "xd",  actions.conflict_choose("none"),   { desc = "Delete the conflict region" } },
            { "n", "1do", actions.diffget("base"),           { desc = "Obtain BASE version of cursor diff" } },
            { "n", "2do", actions.diffget("ours"),           { desc = "Obtain OURS version of cursor diff" } },
            { "n", "3do", actions.diffget("theirs"),         { desc = "Obtain THEIRS version of cursor diff" } },
            { "n", "g?",  actions.help({ "view", "diff3" }), { desc = "Diffview Help" } },
          },
          help_panel = {
            { "n", "q",     actions.close, { desc = "Close help menu" } },
            { "n", "<esc>", actions.close, { desc = "Close help menu" } },
          },
        }
      }
    end,
  }
}
