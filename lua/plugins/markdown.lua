return {
  {
    "iamcco/markdown-preview.nvim",
    version = "*",
    build = ":call mkdp#util#install(v:true)",
    ft = { "markdown" }
  },
  {
    'MeanderingProgrammer/render-markdown.nvim',
    ft = "markdown",
    dependencies = { 'nvim-treesitter/nvim-treesitter', 'nvim-tree/nvim-web-devicons' },
    config = function()
      require("render-markdown").setup {
        sign = {
          enabled = false,
        },
      }
    end
  },
  {
    "tadmccorkle/markdown.nvim",
    ft = "markdown",
    opts = {},
  }
}
