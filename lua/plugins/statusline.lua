local get_lsp_providers = function()
  local clients = vim.lsp.get_clients({ bufnr = 0 })
  local names = {}
  for _, client in pairs(clients) do
    if client.name ~= "null-ls" then
      table.insert(names, client.name)
    end
  end
  return names
end

local get_null_ls_providers = function()
  local sources = require("null-ls.sources")
  local ft = vim.bo.filetype
  local formatters = sources.get_available(ft)
  local names = {}
  for _, formatter in ipairs(formatters) do
    table.insert(names, formatter.name)
  end
  return names
end

local get_providers = function()
  local lsp_providers = get_lsp_providers()
  local null_ls_providers = get_null_ls_providers()
  return vim.list_extend(lsp_providers, null_ls_providers)
end

local function session_name()
  local name = require("sessions").name
  if name then
    return "󰰣 " .. name
  else
    return vim.uv.cwd():gsub(os.getenv("HOME"), "~")
  end
end

return {
  "nvim-lualine/lualine.nvim",
  event = "VeryLazy",
  dependencies = { "nvim-tree/nvim-web-devicons" },
  config = function()
    require("lualine").setup {
      options = {
        section_separators = { left = "", right = "" },
        component_separators = { left = "", right = "" },
        disabled_filetypes = {
          "DiffviewFiles",
        },
      },
      sections = {
        lualine_b = {
          {
            "branch",
            fmt = function(s, _)
              if s:len() > 30 then
                return s:sub(0, 30) .. "..."
              else
                return s
              end
            end
          },
          {
            "diff",
            symbols = {
              added = " ",
              modified = " ",
              removed = " ",
            },
          },
          "diagnostics",
        },
        lualine_c = {
          {
            session_name,
            color = function()
              if require("sessions").name then
                local hl = vim.api.nvim_get_hl(0, { name = "Session" })
                if hl and hl.fg then
                  return { fg = string.format("#%06x", hl.fg) }
                end
              end
            end
          }
        },
        lualine_y = {
          function()
            local providers = get_providers()
            return table.concat(providers, ",")
          end,
        },
      },
      extensions = {
        "neo-tree",
        "fugitive",
        {
          sections = {
            lualine_b = { "mode" }
          },
          filetypes = { "fugitiveblame" }
        },
        "quickfix",
        "man",
        "nvim-dap-ui",
      },
    }
  end
}
