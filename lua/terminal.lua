local M = {}

local tabterms = {}

function M.new()
  local tab = vim.api.nvim_get_current_tabpage()

  local termbuf = vim.api.nvim_create_buf(false, false)
  vim.api.nvim_buf_set_option(termbuf, "filetype", "terminal")

  vim.cmd("botright 15split")
  vim.api.nvim_win_set_option(0, "number", false)
  vim.api.nvim_win_set_buf(0, termbuf)

  local jobid = vim.fn.termopen(vim.o.shell, {
    on_exit = function()
      vim.schedule(function() vim.api.nvim_buf_delete(termbuf, { force = true }) end)
      tabterms[tab] = nil
    end
  })

  local tabnum = vim.api.nvim_tabpage_get_number(tab)
  local augroup = vim.api.nvim_create_augroup("KillTabTerm:" .. tab, { clear = true })
  vim.api.nvim_create_autocmd("TabClosed", {
    group = augroup,
    callback = function(ev)
      if tonumber(ev.file) == tabnum then
        vim.fn.jobstop(jobid)
        vim.api.nvim_del_augroup_by_id(augroup)
      end
    end
  })

  tabterms[tab] = {
    termbuf = termbuf,
    jobid = jobid
  }

  vim.cmd("startinsert")
end

local function find_termwin(termbuf)
  local tab = vim.api.nvim_get_current_tabpage()
  for _, win in ipairs(vim.api.nvim_tabpage_list_wins(tab)) do
    if vim.api.nvim_win_get_buf(win) == termbuf then
      return win
    end
  end
end

function M.toggle()
  local tab = vim.api.nvim_get_current_tabpage()
  local tabterm = tabterms[tab]
  if not tabterm then
    -- no terminal for current tab so create new one
    M.new()
  else
    local termbuf = tabterm.termbuf
    local termwin = find_termwin(termbuf)
    if not termwin then
      -- terminal exists but is not open so open it
      vim.cmd("botright 15split")
      vim.api.nvim_win_set_buf(0, termbuf)
      vim.cmd("startinsert")
    else
      -- terminal exists and is open so close it
      vim.api.nvim_win_close(termwin, true)
    end
  end
end

function M.close_termwin()
  local tab = vim.api.nvim_get_current_tabpage()
  local tabterm = tabterms[tab]

  if tabterm then
    local termwin = find_termwin(tabterms[tab].termbuf)
    if termwin then
      vim.api.nvim_win_close(termwin, true)
    end
  end
end

function M.close_all()
  for _, tabterm in ipairs(tabterms) do
    vim.fn.jobstop(tabterm.jobid)
  end
end

return M
