local M = {}

M.defaults = {
  colorscheme = "catppuccin",
  colorscheme_style = nil,
  guifont = "Fira Code:h13",
  codelens_autorefresh = true,
  java = {},
  tools = {},
}

local function read_settings(path)
  if not vim.uv.fs_stat(path) then
    return {}
  end

  local lines = vim.fn.readfile(path, '')
  if next(lines) == nil then
    return {}
  end

  local s = vim.fn.reduce(lines, function(acc, l) return acc .. l end)
  return vim.json.decode(s)
end

local function get_base_settings()
  local path = vim.fn.expand("~/.config/nvim/settings.json")
  return read_settings(path)
end

local function get_cwd_settings()
  local path = vim.fn.getcwd() .. "/.nvim/settings.json"
  return read_settings(path)
end

local function get_settings()
  return vim.tbl_deep_extend("force", M.defaults, get_base_settings(), get_cwd_settings())
end

function M.reload()
  setmetatable(M, nil)
  setmetatable(M, { __index = get_settings() })
end

function M.edit_global_settings()
  vim.cmd.new("~/.config/nvim/settings.json")
  vim.bo.bufhidden = "delete"
end

function M.ensure_local_config_dir()
  if vim.fn.isdirectory(".nvim") == 0 then
    vim.fn.mkdir(".nvim")
  end
end

function M.edit_local_settings()
  M.ensure_local_config_dir()
  vim.cmd.new(".nvim/settings.json")
  vim.bo.bufhidden = "delete"
end

local config_group = vim.api.nvim_create_augroup("ConfigReload", { clear = true })

vim.api.nvim_create_autocmd({ "TabEnter", "DirChanged" }, {
  group = config_group,
  callback = M.reload
})

vim.api.nvim_create_autocmd("BufWritePost", {
  group = config_group,
  pattern = {
    vim.fn.expand("~/.config/nvim/settings.json"),
    "*/.nvim/settings.json"
  },
  callback = function(ev)
    local file = ev.match
    if file == vim.fn.expand("~/.config/nvim/settings.json")
        or file == vim.fn.getcwd() .. "/.nvim/settings.json" then
      M.reload()
      vim.notify("Reloaded settings", vim.log.levels.WARN)
    end
  end
})

setmetatable(M, { __index = get_settings() })

return M
