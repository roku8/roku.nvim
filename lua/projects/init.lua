local M = {}
local config = require("projects.config")
local home = vim.uv.os_homedir()

function M.find_root(root_markers)
  local current_buf = vim.api.nvim_get_current_buf()
  local buf_name = vim.api.nvim_buf_get_name(current_buf)
  local buf_dir = vim.fs.dirname(buf_name)

  local root_marker = vim.fs.find(root_markers, { path = buf_dir, upward = true, stop = home })[1]
  return vim.fs.dirname(root_marker)
end

local function fd_cmd(opts)
  local cmd = "fd -a --color=never -t f"

  for _, p in ipairs(opts.exclude_patterns) do
    cmd = cmd .. " --exclude " .. p
  end

  local patterns = vim.tbl_map(function(s) return "^" .. s .. "$" end, vim.tbl_keys(opts.root_markers))
  cmd = cmd .. string.format(" '(%s)'", table.concat(patterns, "|"))

  for _, p in ipairs(opts.search_paths) do
    cmd = cmd .. " --search-path " .. p
  end

  cmd = cmd .. " -x dirname"

  return cmd
end

local function fzf_source()
  return function(cb)
    local projects = {}

    vim.fn.jobstart(fd_cmd(config), {
      on_stdout = function(_, dirs, _)
        for _, d in ipairs(dirs) do
          if d == "" then goto continue end

          local pd = vim.fs.dirname(d)
          if projects[d] or projects[pd] then goto continue end

          -- if parent directory is a project then use that instead
          for m, _ in pairs(config.root_markers) do
            if vim.uv.fs_stat(pd .. "/" .. m) then
              d = pd
              break
            end
          end

          projects[d] = true

          local icons = {}
          for m, icon in pairs(config.root_markers) do
            if vim.uv.fs_stat(d .. "/" .. m) then
              icons[icon] = true
            end
          end

          local dir = d:gsub(home, "~")
          local parts = { dir }
          vim.list_extend(parts, vim.tbl_keys(icons))

          cb(table.concat(parts, " "))

          ::continue::
        end
      end,
      on_exit = function(_, _, _) cb() end
    })
  end
end

local function on_select(selected)
  local i, _ = selected[1]:find(" ")
  local dir = selected[1]:sub(1, i - 1)

  local current_buffer = vim.api.nvim_get_current_buf()

  if vim.api.nvim_get_option_value("filetype", { buf = current_buffer }) ~= "alpha" then
    vim.cmd.tabnew()
    current_buffer = vim.api.nvim_get_current_buf() -- delete the temp buffer

    vim.cmd.tcd(dir)
    require("dashboard").setup("project")
    vim.cmd("Alpha")

    vim.api.nvim_buf_delete(current_buffer, { force = true })
  else
    vim.cmd.tcd(dir)
    require("dashboard").setup("project")
    vim.cmd("AlphaRedraw")
  end
end

local fzf_default_opts = {
  prompt  = "❯ ",
  winopts = {
    width = 0.5,
    height = 0.33,
    title = "Projects",
    title_pos = "center"
  },
  actions = {
    default = on_select
  }
}

function M.find_projects(opts)
  local fzf_opts = vim.tbl_extend("force", fzf_default_opts, opts)
  require("fzf-lua").fzf_exec(fzf_source(), fzf_opts)
end

function M.setup(opts)
  config.setup(opts)

  local fzflua = require("fzf-lua")
  fzflua["projects"] = function(...)
    fzflua["projects"] = function(...)
      fzflua.set_info {
        cmd = "projects",
        mod = "projects",
        fnc = "find_projects",
      }
      return require("projects").find_projects(...)
    end
    return fzflua["projects"](...)
  end
end

return M
