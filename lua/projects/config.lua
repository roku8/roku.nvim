local M = {
  root_markers = {
    ["gradlew"] = "",
    ["build.gradle"] = "",
    ["settings.gradle"] = "",
    ["build.gradle.kts"] = "",
    ["settings.gradle.kts"] = "",
    ["mvnw"] = "",
    ["pom.xml"] = "",
    ["Cargo.toml"] = "󱘗",
    ["package.json"] = "󰛷",
    ["Makefile"] = "",
    ["lazy-lock.json"] = "",
  },
  search_paths = { "~", "~/.config/nvim" },
  exclude_patterns = { "node_modules", "Library", "webapps", "Downloads" },
  extend = {
    search_paths = {},
    exclude_patterns = {}
  }
}

function M.setup(opts)
  if opts then
    M = vim.tbl_deep_extend("force", M, opts)
    vim.list_extend(M.search_paths, M.extend.search_paths)
    vim.list_extend(M.exclude_patterns, M.extend.exclude_patterns)
  end
end

return M
