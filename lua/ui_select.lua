local M = {}

function M.setup()
  vim.ui.select = function(items, opts, on_choice)
    require("fzf-lua").register_ui_select(function(ui_opts, _)
      return {
        prompt = "❯ ",
        winopts = {
          title = ui_opts.prompt:gsub(":%s*$", ""),
          title_pos = "center",
          height = 0.33,
          width = 0.5
        }
      }
    end)

    vim.ui.select(items, opts, on_choice)
  end
end

return M
