local M = {}

M.capabilities = require("cmp_nvim_lsp").default_capabilities()
M.full_capabilities = vim.tbl_deep_extend("force", vim.lsp.protocol.make_client_capabilities(), M.capabilities)

function M.default_on_attach(client, bufnr)
  require("keymaps").coding(client, bufnr)

  if client.supports_method("textDocument/codeLens") and require("config").codelens_autorefresh then
    local group = vim.api.nvim_create_augroup("CodelensAutorefresh:" .. bufnr, { clear = true })

    vim.api.nvim_create_autocmd({ "BufEnter", "BufWritePost" }, {
      group = group,
      buffer = bufnr,
      callback = vim.lsp.codelens.refresh
    })

    vim.api.nvim_create_autocmd({ "BufWipeout", "BufDelete", "LspDetach" }, {
      group = group,
      buffer = bufnr,
      callback = function()
        vim.api.nvim_del_augroup_by_id(group)
      end
    })

    vim.lsp.codelens.refresh()
  end

  if client.supports_method("textDocument/inlayHint") or client.server_capabilities.inlayHintProvider then
    vim.lsp.inlay_hint.enable(true, { bufnr = bufnr })
  end
end

M.servers = {
  lua_ls = {
    settings = {
      Lua = {
        runtime = {
          version = "LuaJIT",
        },
        diagnostics = {
          globals = { "vim" },
        },
        completion = {
          callSnippet = "Replace"
        },
      },
    }
  },
  jsonls = {},
  yamlls = {},
  bashls = {},
  gradle_ls = {},
  tsserver = {
    on_attach = function(client, bufnr)
      M.default_on_attach(client, bufnr)
      M.setup_lsp_hls(bufnr, {
        ["@lsp.mod.constant"] = function(token)
          local mod = token.modifiers
          local type = token.type
          return mod.readonly and (type == "property" or type == "variable")
        end
      })
    end
  },
  cssls = {},
  html = {},
  angularls = {
    cmd = {
      "ngserver",
      "--stdio",
      "--tsProbeLocations",
      "",
      "--ngProbeLocations",
      "",
      "--forceStrictTemplates"
    },
    on_new_config = function(new_config, angular_root)
      new_config.cmd = {
        "ngserver",
        "--stdio",
        "--tsProbeLocations",
        angular_root .. "/node_modules",
        "--ngProbeLocations",
        angular_root .. "/node_modules",
        "--forceStrictTemplates"
      }
    end,
    on_attach = function(client, bufnr)
      M.default_on_attach(client, bufnr)

      local ng = require("ng")
      require("which-key").add {
        buffer = bufnr,
        { "gC", ng.goto_component_with_template_file, desc = "Goto Angular Component" },
        { "gT", ng.goto_template_for_component, desc = "Goto Angular Template" },
        { "<leader>lt", ng.get_template_tcb, desc = "Get Angular Template TCB" }
      }

      vim.lsp.commands["angular.goToComponentWithTemplateFile"] = ng.goto_component_with_template_file
      vim.lsp.commands["angular.goToTemplateForComponent"] = ng.goto_template_for_component
      vim.lsp.commands["angular.getTemplateTcb"] = ng.get_template_tcb
    end
  },
  clangd = {},
  basedpyright = {},
}

function M.make_on_attach(on_attach)
  return function(client, bufnr)
    if on_attach then
      on_attach(client, bufnr)
    else
      M.default_on_attach(client, bufnr)
    end

    vim.api.nvim_exec_autocmds("User", {
      pattern = "LspAttachDone",
      data = {
        client_name = client.name,
        bufnr = bufnr
      }
    })
  end
end

function M.after_lsps_attached(bufnr, lsps, cb)
  if not lsps or #lsps < 1 then return end

  local augroup = vim.api.nvim_create_augroup("AfterLspsAttached:" .. bufnr, { clear = true })
  local lsp_attached = coroutine.wrap(function()
    for _ = 1, #lsps - 1 do coroutine.yield() end

    cb()
    vim.api.nvim_del_augroup_by_id(augroup)
  end)

  vim.api.nvim_create_autocmd("User", {
    group = augroup,
    pattern = "LspAttachDone",
    callback = function(args)
      local data = args.data
      if data.bufnr == bufnr and vim.tbl_contains(lsps, data.client_name) then
        lsp_attached()
      end
    end
  })
end

function M.setup_lsp_hls(bufnr, hls)
  if not hls or not next(hls) then return end

  vim.api.nvim_create_autocmd("LspTokenUpdate", {
    group = vim.api.nvim_create_augroup("LspCustomHighlights:" .. bufnr, { clear = true }),
    buffer = bufnr,
    callback = function(ev)
      local token = ev.data.token
      for hl_group, should_hl in pairs(hls) do
        if should_hl(token) then
          vim.lsp.semantic_tokens.highlight_token(
            token,
            bufnr,
            ev.data.client_id,
            hl_group
          )
        end
      end
    end
  })
end

return M
