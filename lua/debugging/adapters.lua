local adapters = {}

local codelldb_path = require("tools_installer").get_install_path("codelldb")
local codelldb_bin = codelldb_path .. "/codelldb"

adapters.codelldb = {
  type = "server",
  port = "${port}",
  executable = {
    command = codelldb_bin,
    args = { "--port", "${port}" },
  }
}

return adapters
