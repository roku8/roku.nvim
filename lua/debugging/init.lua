local M = {}

M.dapui_layouts = {}

M.dapui_layouts.default = {
  controls = {
    enabled = true,
    element = "console",
  },
  layouts = {
    {
      elements = {
        {
          id = "stacks",
          size = 0.31
        },
        {
          id = "scopes",
          size = 0.25
        },
        {
          id = "watches",
          size = 0.25
        },
        {
          id = "breakpoints",
          size = 0.19
        },
      },
      position = "left",
      size = 35
    },
    {
      elements = {
        {
          id = "console",
          size = 1
        },
      },
      position = "bottom",
      size = 0.31
    }
  },
}

M.dapui_layouts.no_breakpoints = {
  controls = {
    enabled = true,
    element = "console",
  },
  layouts = {
    {
      elements = {
        {
          id = "console",
          size = 1
        },
      },
      position = "bottom",
      size = 0.31
    }
  },
}

---@param opts DebugOpts
local function get_launchjson_path(opts)
  local launchjs_path
  if opts.project_root then
    launchjs_path = opts.project_root .. "/.nvim/launch.json"
  else
    launchjs_path = vim.uv.cwd() .. "/.nvim/launch.json"
  end
  return launchjs_path
end

--- @param opts DebugOpts
local function reload_launchjs(opts)
  local launchjs_path = get_launchjson_path(opts)
  if vim.uv.fs_stat(launchjs_path) then
    local types_to_ft = {}
    if opts.adapter then
      local ft = vim.api.nvim_get_option_value("filetype", { buf = opts.buffer })
      types_to_ft[opts.adapter] = { ft }
    end
    require("dap.ext.vscode").load_launchjs(launchjs_path, types_to_ft)
  end
end

local function run_or_debug(opts)
  reload_launchjs(opts)
  require("dap").continue()
end

local function edit_launchjson(opts)
  vim.cmd.new(get_launchjson_path(opts))
  vim.bo.bufhidden = "delete"
end

--- @class DebugOpts
--- @field buffer integer the buffer to setup
--- @field project_root string? the root directory of the project
--- @field adapter string? the dap adapter to use

---Setup debugging keymaps for a buffer
---@param opts DebugOpts
function M.setup(opts)
  opts = opts or {}

  local dapui = require("dapui")
  local dap = require("dap")
  local wk = require("which-key")

  vim.api.nvim_create_autocmd("FileType", {
    pattern = "dapui*,dap-repl",
    callback = function(ev)
      wk.add {
        buffer = ev.buf,
        { "<leader>d",  group = "Debug" },
        { "<leader>du", dapui.close,    desc = "Close Debug UI" },
        { "<leader>dr", dap.repl.close, desc = "Close REPL" },
      }
    end
  })

  wk.add {
    buffer = opts.buffer,
    { "<leader>d",  group = "Debug" },
    { "<leader>dd", function() run_or_debug(opts) end,               desc = "Run/Debug Application" },
    { "<leader>dr", function() dap.repl.toggle({ height = 10 }) end, desc = "Toggle REPL" },
    { "<leader>db", dap.toggle_breakpoint,                           desc = "Toggle Breakpoint" },
    { "<leader>dB", dap.clear_breakpoints,                           desc = "Clear All Breakpoints" },
    { "<leader>du", dapui.toggle,                                    desc = "Toggle Debug UI" },
    { "<leader>de", dapui.eval,                                      desc = "Evaluate Cursor Word" },
    { "<leader>dl", function() edit_launchjson(opts) end,            desc = "Edit launch.json" }
  }
end

return M
