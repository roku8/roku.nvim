local M = {}

function M.setup()
  vim.api.nvim_create_autocmd({ "VimEnter", "TabNewEntered" }, {
    callback = function()
      vim.cmd.clearjumps()
    end
  })
end

return M
