local M = {}

function M.padding(size)
  return {
    type = "padding",
    val = size or 2
  }
end

function M.text(val, hl)
  return {
    type = "text",
    val = val,
    opts = {
      position = "center",
      hl = hl
    }
  }
end

function M.group(layout)
  return {
    type = "group",
    val = layout,
    opts = {
      spacing = 1
    }
  }
end

function M.button(opts)
  local mapping_opts = opts.mapping_opts or { silent = true, nowait = true }
  local keymap = opts.keymap or opts.mapping and { "n", opts.sc, opts.mapping, mapping_opts }

  local on_press = opts.on_press or function()
    local keys = vim.api.nvim_replace_termcodes(opts.sc .. "<Ignore>", true, false, true)
    vim.api.nvim_feedkeys(keys, "t", false)
  end

  return {
    type = "button",
    val = opts.text,
    on_press = on_press,
    opts = {
      position = "center",
      keymap = keymap,
      hl = "AlphaButtons",
      shortcut = opts.sc,
      hl_shortcut = "AlphaShortcut",
      align_shortcut = "right",
      cursor = 0,
      width = 50,
    }
  }
end

function M.header()
  local headers = require("headers")
  local num = math.random(#headers)
  return M.text(headers[num], "AlphaHeader")
end

math.randomseed(os.time())

function M.session_button(session, sc)
  return M.button {
    text = string.format("󰁯 %s", session.name),
    on_press = function()
      require("sessions").load(session.name)
    end,
    sc = sc,
    keymap = {
      "n",
      sc,
      "<cmd>lua require('sessions').load('" .. session.name .. "')<cr>",
      { noremap = true, silent = true, nowait = true }
    }
  }
end

function M.session_buttons(sessions)
  local buttons = {}
  for i, session in ipairs(sessions) do
    table.insert(buttons, M.session_button(session, tostring(i)))
  end

  return buttons
end

function M.session_section()
  local sessions = require("sessions").list()
  if sessions and #sessions > 0 then
    return {
      M.text("Restore Session", "AlphaHeaderLabel"),
      M.padding(1),
      M.group(M.session_buttons(sessions)),
      M.padding(2)
    }
  end
  return {}
end

function M.default_section()
  return {
    M.text("Shortcuts", "AlphaHeaderLabel"),
    M.padding(1),
    M.group {
      M.button {
        text = "󰥨 Find Project",
        sc = "p",
        mapping = "<leader>fp"
      },
      M.button {
        text = "󰈞 Find File",
        sc = "f",
        mapping = "<leader>ff",
      },
      M.button {
        text = "󰈞 Recent File",
        sc = "r",
        mapping = "<leader>fr",
      },
      M.button {
        text = "󰝒 New File",
        sc = "n",
        mapping = "<cmd>enew<cr>"
      },
      M.button {
        text = "󱞊 Explore",
        sc = "e",
        mapping = "<cmd>Neotree current<cr>"
      },
      M.button {
        text = " Quit",
        sc = "q",
        mapping = "<leader>q",
      }
    }
  }
end

function M.get_start_layout()
  local layout = {
    M.padding(2),
    M.header(),
    M.padding(2),
  }

  vim.list_extend(layout, M.session_section())
  vim.list_extend(layout, M.default_section())

  return layout
end

function M.get_project_layout()
  local cwd = vim.fn.getcwd():gsub(os.getenv("HOME"), "~")

  local layout = {
    M.padding(2),
    M.header(),
    M.padding(2),
    M.text(cwd, "AlphaHeaderLabel"),
    M.padding(2),
  }

  vim.list_extend(layout, M.default_section())

  return layout
end

function M.setup(layout)
  local alpha = require("alpha")
  if not layout or layout == "start" then
    alpha.setup {
      layout = M.get_start_layout()
    }
    M._layout = layout
  elseif layout == "project" then
    alpha.setup {
      layout = M.get_project_layout()
    }
    M._layout = layout
  else
    error("No layout: " .. layout, vim.log.levels.ERROR)
  end
end

function M.refresh()
  M.setup(M._layout)
  vim.cmd("AlphaRedraw")
end

return M
