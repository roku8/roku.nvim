local M = {}

function M.update(colorscheme)
  if colorscheme:find("catppuccin", 1, true) then
    local colors = require("catppuccin.palettes").get_palette()
    M.fg = colors.overlay1
    M.hl = colors.peach
    M["fg+"] = colors.text
    M["bg+"] = colors.base
    M["hl+"] = colors.peach
    M.info = colors.subtext0
    M.gutter = colors.base
    M.prompt = colors.peach
    M.pointer = colors.peach
    M.marker = colors.peach
  else
    for key, _ in pairs(M) do
      if key ~= "update" then
        M[key] = nil
      end
    end
  end
end

function M.setup()
  vim.api.nvim_create_autocmd("ColorScheme", {
    callback = function(ev)
      M.update(ev.match)
    end
  })
end

return M
