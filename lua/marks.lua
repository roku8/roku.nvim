local M = {}

---@class Mark
---@field path string path to file
---@field pos table tuple of (row, col) cursor position
---@field entry string mark location as fzf_entry

---@type table<integer, Mark[]> map between tabpage and list of marks for tabpage
M._marks = {}

---@type table<integer, integer> map between tabpage and most recent visited mark in that tabpage
M._current_marks = {}

local mark_actions

function M.add_mark()
  local make_entry = require("fzf-lua.make_entry")
  local bufnr = vim.api.nvim_get_current_buf()
  local cursor = vim.api.nvim_win_get_cursor(0)
  local entry = make_entry.lcol({
    bufnr = bufnr,
    lnum = cursor[1],
    col = cursor[2],
    text = vim.trim(vim.api.nvim_buf_get_lines(bufnr, cursor[1] - 1, cursor[1], false)[1])
  })

  entry = make_entry.file(entry, { file_icons = true, color_icons = true })

  local tab = vim.api.nvim_get_current_tabpage()
  if not M._marks[tab] then
    M._marks[tab] = {}
  end

  local tab_marks = M._marks[tab]
  tab_marks[#tab_marks + 1] = {
    path = vim.api.nvim_buf_get_name(bufnr),
    pos = cursor,
    entry = entry,
  }

  M._current_marks[tab] = #tab_marks

  local bufname = vim.api.nvim_buf_get_name(bufnr)
  vim.notify("Added mark " .. string.format("%s:%d:%d", bufname, cursor[1], cursor[2]), vim.log.levels.INFO)
end

local function init_mark_actions()
  mark_actions = vim.deepcopy(require("fzf-lua").defaults.actions.files)
  local path = require("fzf-lua.path")

  local old_default = mark_actions["default"]
  mark_actions["default"] = function(selected, opts)
    if #selected == 1 then
      local selected_path = path.entry_to_file(selected[1])
      local tab = vim.api.nvim_get_current_tabpage()
      for i, mark in ipairs(M._marks[tab]) do
        local mark_path = path.entry_to_file(mark.entry)
        if mark_path.path == selected_path.path
            and mark_path.line == selected_path.line
            and mark_path.col == selected_path.col then
          M._current_marks[tab] = i
        end
      end
    end
    old_default(selected, opts)
  end

  mark_actions["ctrl-d"] = {
    function(selected)
      for _, m in ipairs(selected) do
        local selected_path = path.entry_to_file(m)
        local tab = vim.api.nvim_get_current_tabpage()
        for i, mark in ipairs(M._marks[tab]) do
          local mark_path = path.entry_to_file(mark.entry)
          if mark_path.path == selected_path.path
              and mark_path.line == selected_path.line
              and mark_path.col == selected_path.col then
            table.remove(M._marks[tab], i)
          end
        end
      end
    end,
    require("fzf-lua.actions").resume
  }
end

function M.goto_mark()
  local tab = vim.api.nvim_get_current_tabpage()
  local tabmarks = M._marks[tab]

  if not tabmarks or next(tabmarks) == nil then
    vim.notify("No available marks", vim.log.levels.WARN)
    return
  end

  local entries = function(fzf_cb)
    for _, m in ipairs(tabmarks) do
      fzf_cb(m.entry)
    end

    fzf_cb()
  end

  if not mark_actions then init_mark_actions() end

  require("fzf-lua").fzf_exec(entries, {
    prompt    = "❯ ",
    previewer = "builtin",
    winopts   = {
      title = "Marks",
      title_pos = "center"
    },
    fzf_opts  = {
      ["--multi"] = true
    },
    actions   = mark_actions
  })
end

function M.cycle()
  local tab = vim.api.nvim_get_current_tabpage()
  local tabmarks = M._marks[tab]

  if not tabmarks or next(tabmarks) == nil then
    vim.notify("No available marks", vim.log.levels.WARN)
    return
  end

  local next_tab_mark = 1 + M._current_marks[tab] % #tabmarks
  local mark = tabmarks[next_tab_mark]

  vim.cmd.e(mark.path)
  vim.api.nvim_win_set_cursor(0, mark.pos)
  vim.api.nvim_feedkeys("zz", "n", true)

  M._current_marks[tab] = next_tab_mark
end

function M.setup()
  vim.api.nvim_create_autocmd("TabClosed", {
    callback = function(_)
      local tab = vim.api.nvim_get_current_tabpage()
      M._marks[tab] = nil
    end
  })
end

return M
