local M = {}
local config = require("config")
local registry = require("mason-registry")

function M.is_installed(tool)
  return registry.is_installed(tool)
end

function M.ensure_installed(tools)
  local to_install = {}

  for _, tool in pairs(tools) do
    local name, version = require("mason-core.package").Parse(tool)

    if not M.is_installed(name) then
      if not version and config.tools[name] then
        tool = name .. "@" .. config.tools[name]
      end

      table.insert(to_install, tool)
    end
  end

  if next(to_install) ~= nil then
    local mason_cmd = { "MasonInstall" }
    for _, tool in ipairs(to_install) do
      table.insert(mason_cmd, tool)
    end
    vim.cmd(table.concat(mason_cmd, " "))
    return false
  end

  return true
end

function M.get_install_path(tool)
  return require("mason-core.path").package_prefix(tool)
end

return M
