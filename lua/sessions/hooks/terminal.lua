local M = {}

function M.before_load()
  require("terminal").close_all()
end

return M
