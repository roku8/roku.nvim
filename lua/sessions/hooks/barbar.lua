local M = {}

function M.before_save()
  vim.api.nvim_exec_autocmds("User", { pattern = "SessionSavePre" })
end

return M
