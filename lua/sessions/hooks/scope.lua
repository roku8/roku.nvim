local M = {}

M.priority = 500

function M.before_save()
  require("scope.core").revalidate()
  return require("scope.session").serialize_state()
end

local function get_loaded_bufs()
  local bufs = vim.api.nvim_list_bufs()
  local bufnames = vim.tbl_map(function(b) return vim.api.nvim_buf_get_name(b) end, bufs)

  -- add reverse lookup
  for i, name in ipairs(bufnames) do
    bufnames[name] = i
  end

  return bufnames
end

local function add_missing_bufs(data)
  local state = vim.json.decode(data)
  local ensure_bufs = vim.tbl_flatten(state.cache or {})
  local loaded = get_loaded_bufs()

  for _, bufname in ipairs(ensure_bufs) do
    if not loaded[bufname] then
      vim.cmd.badd(bufname)
    end
  end
end

function M.after_load(data)
  if data then
    add_missing_bufs(data)
    require("scope.session").deserialize_state(data)
  end
end

return M
