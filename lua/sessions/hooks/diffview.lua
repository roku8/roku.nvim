local M = {}

local function close_tabpage(tabpage)
  for _, win in ipairs(vim.api.nvim_tabpage_list_wins(tabpage)) do
    vim.api.nvim_win_close(win, true)
  end
end

function M.after_load()
  for _, tabpage in ipairs(vim.api.nvim_list_tabpages()) do
    for _, win in ipairs(vim.api.nvim_tabpage_list_wins(tabpage)) do
      local buf = vim.api.nvim_win_get_buf(win)
      local bufname = vim.api.nvim_buf_get_name(buf)
      if bufname:match("diffview://.*") then
        close_tabpage(tabpage)
        break
      end
    end
  end
end

return M
