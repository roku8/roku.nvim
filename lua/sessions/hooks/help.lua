local M = {}

local function is_visible(bufnr)
  for _, win in ipairs(vim.api.nvim_list_wins()) do
    if vim.api.nvim_win_get_buf(win) == bufnr then
      return true
    end
  end
  return false
end

local function is_lazy_plugin_help(bufnr)
  local name = vim.api.nvim_buf_get_name(bufnr)
  local lazy_root = require("lazy.core.config").options.root
  local _, _, plugin = name:find(lazy_root .. "/(.-)/doc/.*")
  if plugin then
    return true, plugin
  else
    return false
  end
end

function M.before_save()
  local preload_plugins = {}

  for _, bufnr in ipairs(vim.api.nvim_list_bufs()) do
    local buftype = vim.api.nvim_get_option_value("buftype", { buf = bufnr })

    if buftype == "help" then
      if not is_visible(bufnr) then
        vim.api.nvim_buf_delete(bufnr, {})
      else
        local is_lazy, plugin = is_lazy_plugin_help(bufnr)
        if is_lazy then
          table.insert(preload_plugins, plugin)
        end
      end
    end
  end

  return {
    preload_plugins = preload_plugins
  }
end

function M.before_load(data)
  if not data or not data.preload_plugins then return end

  for _, plugin in ipairs(data.preload_plugins) do
    vim.cmd("Lazy! load " .. plugin)
  end
end

return M
