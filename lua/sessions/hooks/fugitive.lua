local M = {}

local function buf_get_win(buf)
  for _, win in ipairs(vim.api.nvim_list_wins()) do
    if vim.api.nvim_win_get_buf(win) == buf then
      return win
    end
  end
end

function M.after_load()
  local original_tab = vim.api.nvim_get_current_tabpage()
  local tabs = {}

  for _, buf in ipairs(vim.api.nvim_list_bufs()) do
    local ft = vim.api.nvim_get_option_value("filetype", { buf = buf })
    local name = vim.api.nvim_buf_get_name(buf)

    if ft == "git" and not vim.uv.fs_stat(name) then
      vim.api.nvim_buf_delete(buf, { force = true })
    elseif ft == "fugitiveblame" then
      vim.api.nvim_buf_delete(buf, { force = true })
    elseif ft == "fugitive" then
      local win = buf_get_win(buf)
      if win then
        vim.api.nvim_set_current_win(win)
        table.insert(tabs, vim.api.nvim_get_current_tabpage())
      end
      vim.api.nvim_buf_delete(buf, { force = true })
    end
  end

  for _, tab in pairs(tabs) do
    vim.api.nvim_set_current_tabpage(tab)
    vim.cmd("Git")
  end

  vim.api.nvim_set_current_tabpage(original_tab)
end

return M
