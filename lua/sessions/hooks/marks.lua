local M = {}

function M.before_save()
  return require("marks")._marks
end

function M.after_load(data)
  if data then
    require("marks")._marks = data
  end
end

return M
