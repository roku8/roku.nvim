local path = require("plenary.path")
local hooks = require("sessions.hooks")
local config = require("sessions.config")

local function co_input(opts)
  local co = coroutine.running()
  vim.ui.input(opts, function(i) coroutine.resume(co, i) end)
  return coroutine.yield()
end

local function co_select(choices, opts)
  local co = coroutine.running()
  vim.ui.select(
    choices,
    opts,
    function(choice) coroutine.resume(co, choice) end
  )
  return coroutine.yield()
end

local M = {}

function M.session_info(name)
  local root = path:new(config.dir) / name
  local vimscript = root / "session.vim"
  local data = root / "data.json"

  local stats = vim.uv.fs_stat(tostring(vimscript))

  return {
    name = name,
    root = tostring(root),
    vimscript = tostring(vimscript),
    data = tostring(data),
    created_at = stats and stats.birthtime.sec,
    last_modified_at = stats and stats.mtime.sec
  }
end

function M.save(info)
  info = info or M.info

  if not info then return end

  local data = hooks.apply_before_save(M.hooks)
  vim.cmd.mksession { info.vimscript, bang = true }
  data = hooks.apply_after_save(M.hooks, data)

  vim.fn.writefile({ vim.json.encode(data) }, info.data)

  M.name = info.name
  M.info = M.session_info(info.name)

  vim.notify("Saved session " .. M.name, vim.log.levels.INFO)
end

function M.create()
  local function _create()
    local input_name = co_input {
      prompt = "Session Name:",
      default = vim.fs.basename(vim.fn.getcwd())
    }

    if not input_name then return end

    local name = input_name:gsub("[^%a%d_-]+", "")
    if #name == 0 then
      vim.notify(string.format("'%s' is an invalid name", input_name), vim.log.levels.ERROR)
      return
    end

    local info = M.session_info(name)

    if vim.uv.fs_stat(info.vimscript) then
      local overwrite = vim.fn.input {
        prompt = "Overwrite session '" .. name .. "' y/[n]? ",
        cancelreturn = "n"
      }

      if overwrite == "y" then
        vim.fn.system("rm -rf " .. info.root)
      else
        _create()
        return
      end
    end

    vim.fn.mkdir(info.root, "p")

    M.save(info)
  end

  coroutine.wrap(_create)()
end

function M.save_or_create()
  if not M.name then
    M.create()
  else
    M.save()
  end
end

function M.list(include_current)
  local dir = vim.uv.fs_scandir(config.dir)
  if not dir then
    return {}
  end

  local sessions = {}
  while true do
    local name = vim.uv.fs_scandir_next(dir)
    if not name then break end

    if name ~= M.name or include_current then
      local info = M.session_info(name)
      if vim.uv.fs_stat(info.vimscript) then
        sessions[#sessions + 1] = info
      end
    end
  end

  table.sort(sessions, function(s1, s2)
    return s1.last_modified_at > s2.last_modified_at
  end)

  return sessions
end

function M.load(name)
  local function _load(s_name)
    local info
    if s_name then
      info = M.session_info(s_name)
      if not vim.uv.fs_stat(info.vimscript) then
        vim.notify(string.format("No session named '%s'", s_name), vim.log.levels.ERROR)
        return
      end
    else
      local sessions = M.list()
      if vim.tbl_isempty(sessions) then
        vim.notify("No sessions available to load", vim.log.levels.WARN)
        return
      end

      info = co_select(sessions, {
        prompt = "Load Session",
        format_item = function(s_info) return s_info.name end
      })
    end

    if not info then return end

    local data
    if vim.uv.fs_stat(info.data) then
      local data_lines = vim.fn.readfile(info.data, "")
      local data_json = vim.fn.reduce(data_lines, function(acc, l) return acc .. l end)
      data = vim.json.decode(data_json)
    end

    data = data or {}

    hooks.apply_before_load(M.hooks, data)
    vim.cmd("silent source " .. info.vimscript)
    hooks.apply_after_load(M.hooks, data)

    M.name = info.name
    M.info = info
  end

  M.save()
  coroutine.wrap(_load)(name)
end

function M.detach()
  M.save()
  M.name = nil
  M.info = nil
end

function M.delete()
  local function _delete()
    local sessions = M.list(true)
    if vim.tbl_isempty(sessions) then
      vim.notify("No sessions to delete", vim.log.levels.WARN)
      return
    end

    local info = co_select(sessions, {
      prompt = "Delete Session",
      format_item = function(info) return info.name end
    })

    if not info then return end

    if M.name == info.name then
      M.detach()
    end

    vim.fn.system("rm -rf " .. info.root)

    vim.api.nvim_exec_autocmds("User", {
      pattern = "SessionDeleted"
    })
  end

  coroutine.wrap(_delete)()
end

function M.setup(opts)
  config.setup(opts)

  M.hooks = hooks.load()

  vim.api.nvim_create_autocmd("VimLeavePre", {
    group = vim.api.nvim_create_augroup("SessionAutosave", { clear = true }),
    callback = function() M.save() end
  })
end

return M
