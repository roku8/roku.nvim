local config = require("sessions.config")

local M = {}

function M.apply_before_save(hooks)
  local data = {}

  for _, hook in ipairs(hooks) do
    if hook.before_save then
      data[hook.name] = hook.before_save()
    end
  end

  return data
end

function M.apply_after_save(hooks, data)
  for _, hook in ipairs(hooks) do
    if hook.after_save then
      data[hook.name] = hook.after_save(data[hook.name])
    end
  end

  return data
end

function M.apply_before_load(hooks, data)
  for _, hook in ipairs(hooks) do
    if hook.before_load then
      hook.before_load(data[hook.name])
    end
  end
end

function M.apply_after_load(hooks, data)
  for _, hook in ipairs(hooks) do
    if hook.after_load then
      hook.after_load(data[hook.name])
    end
  end
end

function M.load()
  local hook_files = vim.api.nvim_get_runtime_file("lua/sessions/hooks/*", true)

  local hook_names = vim.tbl_map(
    function(file)
      local name = vim.fs.basename(file):gsub("%.lua", "")
      return name
    end,
    hook_files
  )

  local enabled_hooks = vim.tbl_filter(
    function(name) return config.hooks[name] ~= false end,
    hook_names
  )

  local hooks = vim.tbl_map(
    function(name)
      local hook = require("sessions.hooks." .. name)
      hook.name = name
      hook.priority = hook.priority or 1000

      return hook
    end,
    enabled_hooks
  )

  table.sort(hooks, function(h1, h2)
    return h1.priority < h2.priority
  end)

  return hooks
end

return M
