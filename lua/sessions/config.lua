local defaults = {
  dir = vim.fn.stdpath("state") .. "/sessions",
  hooks = {}
}

local M = setmetatable({}, { __index = defaults })

function M.setup(opts)
  setmetatable(M, nil)
  setmetatable(M, { __index = vim.tbl_deep_extend("force", defaults, opts or {}) })
end

return M
