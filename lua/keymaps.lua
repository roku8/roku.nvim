local M = {}

--- @type wk.Spec
M.defaults = {
  { "<leader>",         group = "Important" },
  { "<leader><leader>", "<cmd>Lazy<cr>",                        desc = "Lazy" },
  { "<leader>w",        "<cmd>wa<cr>",                          desc = "Save All" },
  { "<leader>q",        "<cmd>wqa<cr>",                         desc = "Save All & Quit" },
  { "<leader>/",        "<Plug>CommentaryLine",                 desc = "Toggle Comment Line" },
  { "<leader>;",        group = "Settings" },
  { "<leader>;;",       require("config").edit_global_settings, desc = "Edit Global Settings" },
  { "<leader>;,",       require("config").edit_local_settings,  desc = "Edit Local Settings" },

  { "/",                "<Plug>Commentary",                     desc = "Toggle Comment Selection",  mode = "x" },
  { ">",                ">gv",                                  desc = "Indent Selection Right",    mode = "v" },
  { "<",                "<gv",                                  desc = "Indent Selection Left",     mode = "v" },
  { "]d",               "]c",                                   desc = "Next diff hunk" },
  { "[d",               "[c",                                   desc = "Previous diff hunk" },
  { "<C-e>",            "<cmd>Neotree toggle<cr>",              desc = "Toggle File Explorer" },
  { "<C-h>",            "<C-w>h",                               desc = "Jump To Left Window" },
  { "<C-j>",            "<C-w>j",                               desc = "Jump To Bottom Window" },
  { "<C-k>",            "<C-w>k",                               desc = "Jump To Top Window" },
  { "<C-l>",            "<C-w>l",                               desc = "Jump To Right Window" },
  { "<C-Tab>",          "<cmd>tabnext<cr>",                     desc = "Next tabpage" },
  { "<C-S-Tab>",        "<cmd>tabprev<cr>",                     desc = "Previous tabpage" },
  { "<C-x>",            "<cmd>tabclose<cr>",                    desc = "Close Current Tabpage" },
  { "E",                "g_",                                   desc = "End of line",               mode = { "n", "x" } },
  { "B",                "^",                                    desc = "Start of line (non-blank)", mode = { "n", "x" } },
  { "Q",                "<cmd>q<cr>",                           desc = "Quit Window" },
}

function M.clipboard()
  vim.keymap.set("t", "<C-r>", "'<C-\\><C-n>\"'.nr2char(getchar()).'pi'", { expr = true })

  local osname = vim.uv.os_uname().sysname
  if osname == "Darwin" then
    vim.keymap.set("v", "<D-c>", '"+y', { desc = "Yank/Copy" })
    vim.keymap.set("i", "<D-v>", '<C-r>+', { desc = "Paste" })
    vim.keymap.set("c", "<D-v>", "<C-r>+", { desc = "Paste" })
    vim.keymap.set("t", "<D-v>", "<C-r>+", { remap = true, desc = "Paste" })
  elseif osname == "Linux" and vim.fn.has("gui_running") == 1 then
    vim.keymap.set("v", "<C-c>", '"+y', { desc = "Yank/Copy" })
    vim.keymap.set("i", "<C-v>", '<C-r>+', { desc = "Paste" })
    vim.keymap.set("c", "<C-v>", "<C-r>+", { desc = "Paste" })
    vim.keymap.set("t", "<C-v>", "<C-r>+", { remap = true, desc = "Paste" })
  end
end

--- @type wk.Spec
M.session = {
  { "<leader>s",  group = "Session" },
  { "<leader>ss", function() require("sessions").save_or_create() end, desc = "Save/Create Session" },
  { "<leader>sc", function() require("sessions").create() end,         desc = "Create Session" },
  { "<leader>sl", function() require("sessions").load() end,           desc = "Load A Session" },
  { "<leader>sd", function() require("sessions").delete() end,         desc = "Delete Session" },
  { "<leader>sx", function() require("sessions").detach() end,         desc = "Detach From Session" },
}

--- @type wk.Spec
M.fuzzy_find = {
  { "<leader>f",         group = "Find" },
  { "<leader>ff",        "<cmd>FzfLua files<cr>",                                     desc = "Find File" },
  { "<leader>fr",        "<cmd>FzfLua oldfiles<cr>",                                  desc = "Find Recent File" },
  { "<leader>fg",        "<cmd>FzfLua git_files<cr>",                                 desc = "Find Git File" },
  { "<leader>fb",        "<cmd>FzfLua buffers<cr>",                                   desc = "Find Buffer" },
  { "<leader>fw",        "<cmd>FzfLua live_grep_native<cr>",                          desc = "Find Word/Pattern" },
  { "<leader>fc",        "<cmd>FzfLua grep_cword<cr>",                                desc = "Find Cursor Word" },
  { "<leader>ft",        "<cmd>FzfLua tabs<cr>",                                      desc = "Find Tab" },
  { "<leader>fp",        "<cmd>FzfLua projects<cr>",                                  desc = "Find Project" },
  { "<leader>f<leader>", function() vim.api.nvim_feedkeys(":FzfLua ", "t", true) end, desc = "FzfLua ..." },
}

--- @type wk.Spec
M.buffers = {
  { "<leader>b",  group = "Buffers" },
  { "<leader>bp", "<cmd>BufferPin<cr>",                        desc = "Pin/Unpin Buffer" },
  { "<leader>bl", "<cmd>BufferMoveNext<cr>",                   desc = "Move Buffer Right" },
  { "<leader>bh", "<cmd>BufferMovePrevious<cr>",               desc = "Move Buffer Left" },
  { "<leader>bc", "<cmd>BufferClose<cr>",                      desc = "Close Buffer" },
  { "<leader>bC", "<cmd>BufferCloseAllButCurrentOrPinned<cr>", desc = "Close All Buffers Except Current/Pinned" },
  { "<leader>bL", "<cmd>BufferCloseBuffersRight<cr>",          desc = "Close Buffers On The Right" },
  { "<leader>bH", "<cmd>BufferCloseBuffersLeft<cr>",           desc = "Close Buffers On The Left" },
  { "<leader>b1", "<cmd>BufferGoto 1<cr>",                     desc = "Go To Buffer 1" },
  { "<leader>b2", "<cmd>BufferGoto 2<cr>",                     desc = "Go To Buffer 2" },
  { "<leader>b3", "<cmd>BufferGoto 3<cr>",                     desc = "Go To Buffer 3" },
  { "<leader>b4", "<cmd>BufferGoto 4<cr>",                     desc = "Go To Buffer 4" },
  { "<leader>b5", "<cmd>BufferGoto 5<cr>",                     desc = "Go To Buffer 5" },
  { "<leader>b6", "<cmd>BufferGoto 6<cr>",                     desc = "Go To Buffer 6" },
  { "<leader>b7", "<cmd>BufferGoto 7<cr>",                     desc = "Go To Buffer 7" },
  { "<leader>b8", "<cmd>BufferGoto 8<cr>",                     desc = "Go To Buffer 8" },
  { "<leader>b9", "<cmd>BufferGoto 9<cr>",                     desc = "Go To Buffer 9" },
  { "<leader>b0", "<cmd>BufferLast<cr>",                       desc = "Go To Last Buffer" },
  { "<M-l>",      "<cmd>BufferNext<cr>",                       desc = "Next Buffer" },
  { "<M-h>",      "<cmd>BufferPrevious<cr>",                   desc = "Previous Buffer" },
  { "<M-Right>",  "<cmd>BufferScrollRight 10<cr>",             desc = "Bufferline Scroll Right" },
  { "<M-Left>",   "<cmd>BufferScrollLeft 10<cr>",              desc = "Bufferline Scroll Left" },
  { "<C-c>",      "<cmd>BufferClose<cr>",                      desc = "Close Current Buffer" }
}

--- @type wk.Spec
M.terminal = {
  { "<C-t>",     function() require("terminal").toggle() end, desc = "Toggle Terminal", mode = { "n", "t" } },
  { "<esc>",     "<C-\\><C-n>",                               mode = "t" },
  { "<S-Space>", "<Space>",                                   mode = "t" },
  { "<S-CR>",    "<CR>",                                      mode = "t" },
  { "<S-BS>",    "<BS>",                                      mode = "t" },
}

--- @type wk.Spec
M.marks = {
  { "<leader>'",   group = "Marks" },
  { "<leader>''",  function() require("marks").add_mark() end,  desc = "Add Mark" },
  { "<leader>'\"", function() require("marks").goto_mark() end, desc = "Goto Mark" },
  { "<C-'>",       function() require("marks").cycle() end,     desc = "Cycle Through Marks" }
}

--- @type wk.Spec
M.git = {
  { "]g",                "<cmd>Gitsigns next_hunk<cr>",                            desc = "Next Git Hunk" },
  { "[g",                "<cmd>Gitsigns prev_hunk<cr>",                            desc = "Previous Git Hunk" },
  { "<leader>g",         group = "Git" },
  { "<leader>gg",        "<cmd>Git<cr>",                                           desc = "Git Status" },
  { "<leader>gc",        "<cmd>Git commit<cr>",                                    desc = "Git Commit" },
  { "<leader>gl",        "<cmd>Git! pull --rebase<cr>",                            desc = "Git Pull (rebase)" },
  { "<leader>gp",        "<cmd>Git! push<cr>",                                     desc = "Git Push" },
  { "<leader>gP",        "<cmd>Git! push --set-upstream origin HEAD<cr>",          desc = "Git Push (create remote branch)" },
  { "<leader>gf",        "<cmd>Git! fetch --prune --tags<cr>",                     desc = "Git fetch (prune & tags)" },
  { "<leader>gj",        "<cmd>Gitsigns next_hunk<cr>",                            desc = "Next Hunk" },
  { "<leader>gk",        "<cmd>Gitsigns prev_hunk<cr>",                            desc = "Previous Hunk" },
  { "<leader>gh",        "<cmd>Gitsigns preview_hunk<cr>",                         desc = "Preview Hunk" },
  { "<leader>gB",        "<cmd>Gitsigns blame_line<cr>",                           desc = "Git Blame Line" },
  { "<leader>gs",        "<cmd>Gitsigns stage_hunk<cr>",                           desc = "Stage Hunk" },
  { "<leader>gr",        "<cmd>Gitsigns reset_hunk<cr>",                           desc = "Reset Hunk" },
  { "<leader>gR",        "<cmd>Gitsigns reset_buffer<cr>",                         desc = "Reset Buffer" },
  { "<leader>gb",        "<cmd>FzfLua git_branches<cr>",                           desc = "Git Branches" },
  { "<leader>g<leader>", function() vim.api.nvim_feedkeys(":Git ", "t", true) end, desc = "Git ..." },
  { "<leader>gd",        group = "Diff" },
  { "<leader>gdv",       "<cmd>DiffviewOpen<cr>",                                  desc = "Open Git Diff" },
  { "<leader>gdh",       "<cmd>DiffviewFileHistory<cr>",                           desc = "View Git History" },
  { "<leader>gdf",       "<cmd>DiffviewFileHistory %<cr>",                         desc = "View File Git History" },
  { "<leader>gdc",       "<cmd>DiffviewClose<cr><cmd>tabprev<cr>",                 desc = "Close Git Diff" },
  { "<leader>gdr",       "<cmd>DiffviewRefresh<cr>",                               desc = "Refresh Git Diff" },
}

local function goto_definition()
  require("fzf-lua").lsp_definitions {
    prompt = "❯ ",
    winopts = {
      title = "LSP Definitions",
      position = "center"
    }
  }
end

local function goto_type_definition()
  require("fzf-lua").lsp_typedefs {
    prompt = "❯ ",
    winopts = {
      title = "LSP Type Definitions",
      position = "center"
    }
  }
end

local function goto_references()
  require("fzf-lua").lsp_references {
    prompt = "❯ ",
    winopts = {
      title = "LSP References",
      position = "center"
    }
  }
end

local function goto_implementations()
  require("fzf-lua").lsp_implementations {
    prompt = "❯ ",
    winopts = {
      title = "LSP Implementations",
      position = "center"
    }
  }
end

function M.coding(_, bufnr)
  --- @type wk.Parse
  require("which-key").add {
    buffer = bufnr,
    { "g",          name = "Goto" },
    { "gd",         goto_definition,                                                desc = "Goto Definition" },
    { "gt",         goto_type_definition,                                           desc = "Goto Type Definition" },
    { "gr",         goto_references,                                                desc = "Goto references" },
    { "gi",         goto_implementations,                                           desc = "Goto Implementation" },
    { "<leader>e",  group = "Errors" },
    { "<leader>ej", vim.diagnostic.goto_next,                                       desc = "Next Error" },
    { "<leader>ek", vim.diagnostic.goto_prev,                                       desc = "Previous Error" },
    { "<leader>el", vim.diagnostic.open_float,                                      desc = "Line Diagnostics" },
    { "<leader>eb", "<cmd>FzfLua diagnostics_document<cr>",                         desc = "Buffer Diagnostics" },
    { "<leader>ew", "<cmd>FzfLua diagnostics_workspace<cr>",                        desc = "Workspace Diagnostics" },
    { "]e",         function() vim.diagnostic.goto_next { severity = "ERROR" } end, desc = "Next Error" },
    { "[e",         function() vim.diagnostic.goto_prev { severity = "ERROR" } end, desc = "Previous Error" },
    { "<leader>l",  group = "LSP" },
    { "<leader>lr", vim.lsp.buf.rename,                                             desc = "Rename Cursor Symbol" },
    { "<leader>la", "<cmd>FzfLua lsp_code_actions<cr>",                             desc = "Code Action" },
    { "<leader>lf", function() vim.lsp.buf.format { timeout_ms = 5000 } end,        desc = "Format Buffer" },
    { "<leader>ls", "<cmd>FzfLua lsp_document_symbols<cr>",                         desc = "Search Buffer Symbol" },
    { "<leader>lS", "<cmd>FzfLua lsp_workspace_symbols<cr>",                        desc = "Search Workspace Symbol" },
    { "<leader>ll", vim.lsp.codelens.refresh,                                       desc = "Refresh Codelens" },
    { "<leader>lL", vim.lsp.codelens.run,                                           desc = "Execute Codelens" },
    { "K",          vim.lsp.buf.hover,                                              desc = "Show Documentation" },
    { "<C-k>",      vim.lsp.buf.signature_help,                                     desc = "Show Signature",         mode = "i" }
  }
end

function M.setup()
  local wk = require("which-key")
  wk.add(M.defaults)
  wk.add(M.session)
  wk.add(M.git)
  wk.add(M.fuzzy_find)
  wk.add(M.buffers)
  wk.add(M.terminal)
  wk.add(M.marks)
  M.clipboard()
end

return M
