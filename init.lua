local config = require("config")

vim.g.mapleader = " "
vim.g.neovide_input_macos_alt_is_meta = true
vim.g.neovide_input_use_logo = true

vim.o.autoindent = true
vim.o.expandtab = true
vim.o.shiftwidth = 2
vim.o.tabstop = 2
vim.o.timeout = true
vim.o.timeoutlen = 500
vim.o.updatetime = 100
vim.o.number = true
vim.o.termguicolors = true
vim.o.clipboard = "unnamedplus"
vim.o.wrap = false
vim.o.cursorline = true
vim.o.jumpoptions = "stack"
vim.o.sessionoptions = "buffers,curdir,globals,localoptions,help,tabpages,winsize"
vim.o.guifont = config.guifont
vim.o.grepprg = "rg --vimgrep -- $*"

local lazypath = vim.fn.expand("~/.local/share/nvim/lazy/lazy.nvim")
if not vim.uv.fs_stat(lazypath) then
  vim.notify("please run bootstrap.sh", vim.log.levels.ERROR)
  return
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup("plugins", {
  root = vim.fn.expand("~/.local/share/nvim/lazy"),
  defaults = {
    lazy = true
  },
  ui = {
    border = "rounded",
    backdrop = 100,
  },
  change_detection = {
    enabled = false,
    notify = false
  }
})

require("sessions").setup()
require("ui_select").setup()
require("fzf_colors").setup()
require("nojumpbleed").setup()
require("marks").setup()

vim.cmd.colorscheme(config.colorscheme)
